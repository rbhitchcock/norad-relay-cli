package main

import (
	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func logsHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	logger := relay.RelayLogger{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
		Follow:        c.Bool("follow"),
		ContainerName: c.String("container-name"),
	}
	return relay.PrintLogs(logger)
}
