package main

import (
	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func enableHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	enabler := relay.RelayEnabler{
		Config: relay.EnableConfig{
			ConfigDir: c.String("dir"),
			Force:     c.Bool("force"),
		},
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}
	return relay.Enable(enabler)
}
