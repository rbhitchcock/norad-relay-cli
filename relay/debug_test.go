package relay

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/docker/docker/api/types"
	"gopkg.in/yaml.v2"
)

type MockDebugger struct {
	mock.Mock
}

func (i *MockDebugger) LoadConfig() (ComposeConfig, error) {
	args := i.Called()
	return args.Get(0).(ComposeConfig), args.Error(1)
}

func (i *MockDebugger) WriteConfig(ComposeConfig) error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockDebugger) CleanupExited() error {
	args := i.Called()
	return args.Error(0)
}

func TestToggleDebug(t *testing.T) {
	cases := []struct {
		load_config  error
		write_config error
		restart      error
		cleanup      error
		expectation  error
	}{
		{errors.New("1"), nil, nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, nil, errors.New("2")},
		{nil, nil, errors.New("3"), nil, errors.New("3")},
		{nil, nil, nil, errors.New("4"), errors.New("4")},
		{nil, nil, nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockDebugger)
			testObj.On("LoadConfig").Return(ComposeConfig{}, c.load_config)
			testObj.On("WriteConfig").Return(c.write_config)
			testObj.On("CleanupExited").Return(c.cleanup)

			disabler := new(MockDisabler)
			disabler.On("FindContainer").Return("foo", c.restart)
			disabler.On("StopContainer").Return(nil)
			disabler.On("RemoveContainer").Return(nil)

			enabler := new(MockEnabler)
			enabler.On("ValidateInstall").Return(c.restart)
			enabler.On("CheckForExisting").Return(nil)
			enabler.On("LoadContainerConfig").Return(ComposeService{}, nil)
			enabler.On("CreateContainer").Return("foo", nil)
			enabler.On("StartContainer").Return(nil)

			ret := ToggleDebug(testObj, enabler, disabler)

			if c.expectation != nil && ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestRelayDebuggerWriteConfig(t *testing.T) {
	config_dir := "/etc/invalid/does/not/exist"
	debugger := RelayDebugger{ConfigDir: config_dir}
	service := ComposeService{}
	services_map := make(map[string]ComposeService)
	services_map["relay"] = service
	config := ComposeConfig{Services: services_map}
	ret := debugger.WriteConfig(config)
	if !strings.Contains(ret.Error(), "no such file") {
		t.Errorf("Expected error to occur be returned while writing file to nonexistent directory.")
	}
}

func TestRelayDebuggerInvalidConfig(t *testing.T) {
	config_dir := "/etc/relay_test_dir"
	debugger := RelayDebugger{ConfigDir: config_dir}
	ret := debugger.WriteConfig(ComposeConfig{})
	if !strings.Contains(ret.Error(), "No Relay service") {
		t.Errorf("Expected to detect an empty services map, but encounted %s instead.", ret)
	}
}

func TestRelayDebuggerWriteConfigYAML(t *testing.T) {
	config_dir := "/etc/relay_test_dir"

	// missing and enabled
	// missing and disabled
	// prod_mode and enabled
	// prod_mode and disabled
	// debug_mode and enabled
	// debug_mode and disabled
	cases := []struct {
		test_case        int
		existing_config  bool
		current_mode     string
		enable           bool
		expected_enabled bool
	}{
		{0, false, "", true, true},
		{1, false, "", false, false},
		{2, true, "production", true, true},
		{3, true, "production", false, false},
		{4, true, "debug", true, true},
		{5, true, "debug", false, false},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing debug mode toggling for test case %d", c.test_case), func(t *testing.T) {
			err := os.Mkdir(config_dir, 0700)
			defer os.RemoveAll(config_dir)
			if err != nil {
				t.Errorf("Test set up failed: %s", err)
			}

			var env []string
			found_mode := ""

			if c.existing_config {
				env = append(env, fmt.Sprintf("RELAY_MODE=%s", c.current_mode))
			} else {
				env = append(env, "FOO=bar")
			}

			service := ComposeService{
				Environment: env,
			}
			config := ComposeConfig{Services: make(map[string]ComposeService)}
			config.Services["relay"] = service

			debugger := RelayDebugger{ConfigDir: config_dir, Enable: c.enable}
			debugger.WriteConfig(config)

			loaded_config, err := debugger.LoadConfig()
			if err != nil {
				t.Errorf("Test set up failed: %s", err)
			}

			loaded_env := loaded_config.Services["relay"].Environment

			for _, e := range loaded_env {
				if strings.Contains(e, "RELAY_MODE") {
					found_mode = e
					break
				}
			}

			if c.expected_enabled {
				if found_mode != "RELAY_MODE=debug" {
					t.Errorf("Expected debug mode to be enabled, got: %s", found_mode)
				}
			} else {
				if found_mode != "RELAY_MODE=production" {
					t.Errorf("Expected debug mode to be disabled, got: %s", found_mode)
				}
			}
		})
	}
}

func TestRelayDebuggerLoadConfig(t *testing.T) {
	// file dne
	// file is invalid
	// file is valid
	dne_dir := "/etc/dne_relay_test_dir"
	invalid_dir := "/etc/invalid_relay_test_dir"
	valid_dir := "/etc/valid_relay_test_dir"
	unsupported_dir := "/etc/unsupported_relay_test_dir"

	os.Mkdir(dne_dir, 0700)
	os.Mkdir(invalid_dir, 0700)
	os.Mkdir(valid_dir, 0700)
	os.Mkdir(unsupported_dir, 0700)

	defer os.RemoveAll(valid_dir)
	defer os.RemoveAll(unsupported_dir)
	defer os.RemoveAll(dne_dir)
	defer os.RemoveAll(invalid_dir)

	compose_valid := fmt.Sprintf("%s/relay.yml", valid_dir)
	compose_unsupported := fmt.Sprintf("%s/relay.yml", unsupported_dir)
	compose_invalid := fmt.Sprintf("%s/relay.yml", invalid_dir)

	service := make(map[string]ComposeService)
	service["relay"] = ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
	}
	config := ComposeConfig{
		Version:  "3",
		Services: service,
	}

	d, _ := yaml.Marshal(&config)

	ioutil.WriteFile(compose_valid, d, 0600)
	ioutil.WriteFile(compose_invalid, [](byte)("foobar\n"), 0600)
	ioutil.WriteFile(compose_unsupported, [](byte)("foo: bar\n"), 0600)

	cases := []struct {
		dir      string
		expected error
		panic    bool
	}{
		{valid_dir, nil, false},
		{invalid_dir, errors.New("yaml: unmarshal errors"), false},
		{dne_dir, errors.New("no such file or directory"), false},
		{unsupported_dir, errors.New(""), true},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Validating install to %s", c.dir), func(t *testing.T) {
			debugger := RelayDebugger{
				ConfigDir: c.dir,
			}
			_, ret := debugger.LoadConfig()

			if c.panic {
				defer func() {
					if r := recover(); r == nil {
						t.Errorf("Unsupported YAML should trigger a panic.")
					}
				}()
			}

			if c.expected == nil {
				if ret != nil {
					t.Error("Expected nil, got", ret)
				}
			} else {
				if !strings.Contains(ret.Error(), c.expected.Error()) {
					t.Errorf("Expected %s, got %s", c.expected, ret)
				}
			}
		})
	}
}

func TestRelayDebuggerCleanupExited(t *testing.T) {
	cases := []struct {
		empty          bool
		retrieve_error error
		remove_error   error
	}{
		{true, nil, nil},
		{false, errors.New("1"), nil},
		{false, nil, errors.New("2")},
		{false, nil, nil},
	}

	for _, c := range cases {
		var containers []types.Container

		if c.empty {
			containers = []types.Container{}
		} else {
			containers = []types.Container{types.Container{}}
		}
		docker := new(MockDocker)
		docker.On("RetrieveTests").Return(containers, c.retrieve_error)
		docker.On("RemoveContainers").Return(c.remove_error)
		debugger := RelayDebugger{
			Docker: docker,
		}

		debugger.Enable = true
		debugger.Cleanup = true
		ret := debugger.CleanupExited()

		if ret != nil {
			t.Errorf("No cleanup should be performed when enabling debug mode. %s", ret)
		}

		debugger.Enable = false
		debugger.Cleanup = false
		ret = debugger.CleanupExited()

		if ret != nil {
			t.Errorf("No cleanup should be performed when clean is disabled. %s", ret)
		}

		debugger.Enable = false
		debugger.Cleanup = true
		ret = debugger.CleanupExited()
		if (c.retrieve_error != nil || c.remove_error != nil) && ret == nil {
			t.Errorf("Error expected, got nil")
		} else if c.retrieve_error == nil && c.remove_error == nil && ret != nil {
			t.Errorf("No error expected, got %s", ret)
		}
	}
}
