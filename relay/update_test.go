package relay

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockUpdater struct {
	mock.Mock
}

func (e *MockUpdater) CleanUp() error {
	args := e.Called()
	return args.Error(0)
}

func TestUpdate(t *testing.T) {
	cases := []struct {
		installer   error
		enabler     error
		disabler    error
		updater     error
		expectation error
	}{
		{installer: errors.New("1"), expectation: errors.New("1")},
		{enabler: errors.New("2"), expectation: errors.New("2")},
		{disabler: errors.New("3"), expectation: errors.New("3")},
		{updater: errors.New("4")},
		{nil, nil, nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			installer := new(MockInstaller)
			installer.On("PullImage").Return(c.installer)

			disabler := new(MockDisabler)
			disabler.On("FindContainer").Return("foo", c.disabler)
			disabler.On("StopContainer").Return(nil)
			disabler.On("RemoveContainer").Return(nil)

			enabler := new(MockEnabler)
			enabler.On("ValidateInstall").Return(c.enabler)
			enabler.On("CheckForExisting").Return(nil)
			enabler.On("LoadContainerConfig").Return(ComposeService{}, nil)
			enabler.On("CreateContainer").Return("foo", nil)
			enabler.On("StartContainer").Return(nil)

			updater := new(MockUpdater)
			updater.On("CleanUp").Return(c.updater)

			ret := Update(updater, installer, enabler, disabler)

			if c.expectation != nil && (ret == nil || ret.Error() != c.expectation.Error()) {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestRelayUpdaterCleanUp(t *testing.T) {
	cases := []struct {
		imageName     string
		imageTag      string
		listError     error
		removeError   error
		expectedError error
	}{
		{imageName: "relay", imageTag: "latest"},
		{imageName: "norad/relay", imageTag: "latest"},
		{imageName: "host.com/relay", imageTag: "latest"},
		{imageName: "host.com/norad/relay", imageTag: "latest"},
		{imageName: "host.com:80/relay", imageTag: "latest"},
		{imageName: "host.com:80/norad/relay", imageTag: "latest"},
		{imageName: "relay"},
		{imageName: "norad/relay"},
		{imageName: "host.com/relay"},
		{imageName: "host.com/norad/relay"},
		{imageName: "host.com:80/relay"},
		{imageName: "host.com:80/norad/relay"},
		{listError: errors.New("1"), expectedError: errors.New("1")},
		{removeError: errors.New("2"), expectedError: errors.New("2")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing image cleanup for %s and %s", c.imageName, c.imageTag), func(t *testing.T) {
			image_list := []types.ImageSummary{{ID: "cleanup_test"}}
			client := MockDockerClient{}
			client.On(
				"ImageRemove",
				context.Background(),
				"cleanup_test",
				types.ImageRemoveOptions{},
			).Return([]types.ImageDeleteResponseItem{}, c.removeError)
			client.On(
				"ImageList",
				context.Background(),
				mock.MatchedBy(
					func(input types.ImageListOptions) bool {
						dangling_filter := input.Filters.Get("dangling")
						ref_filter := input.Filters.Get("reference")
						return ref_filter[0] == c.imageName && dangling_filter[0] == "true"
					},
				),
			).Return(image_list, c.listError)

			name_with_tag := c.imageName
			if c.imageTag != "" {
				name_with_tag = fmt.Sprintf("%s:%s", c.imageName, c.imageTag)
			}

			updater := RelayUpdater{
				Image:  name_with_tag,
				Docker: RelayDocker{Client: &client},
			}
			ret := updater.CleanUp()

			if c.expectedError != nil {
				if ret == nil || ret.Error() != c.expectedError.Error() {
					t.Errorf("Expected to return error %s, but got %s", c.expectedError, ret)
				}
			}
		})
	}
}
