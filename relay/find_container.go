package relay

import (
	"context"
	"log"

	"github.com/docker/docker/api/types"
)

// findRelayContainer is a convenience method for locating the existence of the Norad Relay container
func findRelayContainer(client RelayDockerClient, only_running bool) (string, error) {
	log.Print("Checking for an existing Norad Relay container.")
	containers, err := client.ContainerList(context.Background(), types.ContainerListOptions{All: !only_running})
	if err != nil {
		log.Printf("The following error occurred while looking for an existing container: %s.", err)
		return "", err
	}

	for i := range containers {
		for _, name := range containers[i].Names {
			if name == "/norad_relay" {
				log.Printf("Norad Relay container %s found.", containers[i].ID)
				return containers[i].ID, nil
			}
		}
	}

	return "", nil
}
