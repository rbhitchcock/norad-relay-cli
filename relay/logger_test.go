package relay

import (
	"context"
	"errors"
	"fmt"
	"io"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockLogger struct {
	mock.Mock
}

func (l *MockLogger) ReadLogs() (io.ReadCloser, error) {
	args := l.Called()
	return args.Get(0).(io.ReadCloser), args.Error(1)
}

func (l *MockLogger) Write(msg []byte) (int, error) {
	args := l.Called()
	return args.Int(0), args.Error(1)
}

type TestReadCloser struct {
	mock.Mock
}

func (t *TestReadCloser) Read(p []byte) (int, error) {
	args := t.Called()
	return args.Int(0), args.Error(1)
}

func (t *TestReadCloser) Close() error {
	args := t.Called()
	return args.Error(0)
}

func TestPrintLogs(t *testing.T) {
	cases := []struct {
		read_logs   error
		write       error
		expectation error
	}{
		{errors.New("1"), nil, errors.New("1")},
		{nil, errors.New("2"), errors.New("2")},
		{nil, io.EOF, io.EOF},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution error detection for method %s", c.expectation), func(t *testing.T) {
			read_closer := new(TestReadCloser)
			read_closer.On("Read").Return(8, nil)
			read_closer.On("Close").Return(nil)

			logger := new(MockLogger)
			logger.On("ReadLogs").Return(read_closer, c.read_logs)
			logger.On("Write").Return(0, c.write)

			ret := PrintLogs(logger)

			if c.expectation != nil && ret == nil {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestRelayLoggerWrite(t *testing.T) {
	cases := []struct {
		msg            []byte
		expected_bytes int
		expected_err   error
	}{
		{[]byte{}, 0, nil},
		{[]byte{0}, 1, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing byte count return for %d", c.expected_bytes), func(t *testing.T) {
			logger := RelayLogger{}
			bytes_read, err := logger.Write(c.msg)
			if bytes_read != c.expected_bytes {
				t.Errorf("Expected to have read %d bytes, but read %d instead.", bytes_read, c.expected_bytes)
			}

			if err != nil {
				t.Errorf("No error should ever be returned, instead %s was returned.", err)
			}

		})
	}
}

func TestRelayReadLogs(t *testing.T) {
	cases := []struct {
		container_name string
		follow         bool
	}{
		{"foo", true},
		{"bar", false},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing log parameters with follow set to %t", c.follow), func(t *testing.T) {
			logger := RelayLogger{Follow: c.follow, ContainerName: c.container_name}
			client := MockDockerClient{}
			client.On(
				"ContainerLogs",
				context.Background(),
				c.container_name,
				types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true, Follow: c.follow},
			).Return(&TestReadCloser{}, nil)
			logger.Docker = RelayDocker{Client: &client}
			logger.ReadLogs()
		})
	}
}
