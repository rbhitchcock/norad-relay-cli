package relay

import (
	"errors"
	"fmt"
	"testing"
)

func TestReset(t *testing.T) {
	cases := []struct {
		installer   error
		uninstaller error
		disabler    error
		expectation error
	}{
		{errors.New("1"), nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, errors.New("2")},
		{nil, nil, errors.New("3"), errors.New("3")},
		{nil, nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			disabler := new(MockDisabler)
			disabler.On("FindContainer").Return("foo", c.disabler)
			disabler.On("StopContainer").Return(nil)
			disabler.On("RemoveContainer").Return(nil)

			installer := new(MockInstaller)
			installer.On("ValidateConfig").Return(c.installer)
			installer.On("ValidateVersion").Return(nil)
			installer.On("PullImage").Return(nil)
			installer.On("CreateDir").Return(nil)
			installer.On("GenerateComposeConfig").Return(nil)
			installer.On("GenerateRelayConfig").Return(nil)
			installer.On("enable").Return(nil)

			uninstaller := new(MockUninstaller)
			uninstaller.On("Disable").Return(c.uninstaller)
			uninstaller.On("RemoveImage").Return(nil)
			uninstaller.On("RemoveConfigDir").Return(nil)

			ret := Reset(installer, uninstaller, disabler)

			if c.expectation != nil && ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}
