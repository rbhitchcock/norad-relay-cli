package relay

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
	"gopkg.in/yaml.v2"
)

type MockPullResp struct {
	s        string
	i        int64
	prevRune int
}

func (r *MockPullResp) Read(b []byte) (n int, err error) {
	if r.i == -1 {
		return 0, errors.New("Invalid response!")
	}

	if r.i >= int64(len(r.s)) {

		return 0, io.EOF

	}

	r.prevRune = -1

	n = copy(b, r.s[r.i:])

	r.i += int64(n)
	return
}

func (r *MockPullResp) Close() error {
	return nil
}

type MockInstaller struct {
	mock.Mock
}

func (i *MockInstaller) PullImage() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) ValidateConfig() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) ValidateVersion() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) CreateDir() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) GenerateComposeConfig() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) GenerateRelayConfig() error {
	args := i.Called()
	return args.Error(0)
}

func (i *MockInstaller) enable() error {
	args := i.Called()
	return args.Error(0)
}

func TestInstall(t *testing.T) {
	cases := []struct {
		validate_config         error
		validate_version        error
		pull_image              error
		create_dir              error
		generate_compose_config error
		generate_relay_config   error
		expectation             error
	}{
		{errors.New("1"), nil, nil, nil, nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, nil, nil, nil, errors.New("2")},
		{nil, nil, errors.New("3"), nil, nil, nil, errors.New("3")},
		{nil, nil, nil, errors.New("4"), nil, nil, errors.New("4")},
		{nil, nil, nil, nil, errors.New("5"), nil, errors.New("5")},
		{nil, nil, nil, nil, nil, errors.New("6"), errors.New("6")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockInstaller)
			testObj.On("ValidateConfig").Return(c.validate_config)
			testObj.On("ValidateVersion").Return(c.validate_version)
			testObj.On("PullImage").Return(c.pull_image)
			testObj.On("CreateDir").Return(c.create_dir)
			testObj.On("GenerateComposeConfig").Return(c.generate_compose_config)
			testObj.On("GenerateRelayConfig").Return(c.generate_relay_config)
			ret := Install(testObj)
			if ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			}
		})
	}
}

func TestRelayInstallerValidateConfig(t *testing.T) {
	valid_relay_dir := "/foo/bar/made/up" // should never exist

	cases := []struct {
		install_dir string
		expected    error
	}{
		{"/", &relayDirExistsError{"/"}},
		{valid_relay_dir, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("install dir: %s", c.install_dir), func(t *testing.T) {
			installer := RelayInstaller{
				Config: InstallConfig{
					InstallDir: c.install_dir,
				},
			}
			err := installer.ValidateConfig()
			if c.expected == nil {
				if err != nil {
					t.Error("Expected nil, got", err)
				}
			} else {
				if err == nil {
					t.Errorf("Expected %s, got nil", err)
				}
			}
		})
	}
}

func TestRelayInstallerValidateVersion(t *testing.T) {
	minimum_version := "1.13.0-0"
	supported_version := "17.06.0-ce"
	deprecated_version := "1.12.0-0"

	cases := []struct {
		server_ver string
		expected   error
	}{
		{supported_version, nil},
		{deprecated_version, &unsupportedDockerVersionError{minimum_version, deprecated_version}},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("testing version %s", c.server_ver), func(t *testing.T) {
			installer := RelayInstaller{
				Docker: RelayDocker{
					MinVer:    minimum_version,
					ServerVer: c.server_ver,
				},
			}
			err := installer.ValidateVersion()
			if c.expected == nil {
				if err != nil {
					t.Error("Expected nil, got", err)
				}
			} else {
				if err == nil {
					t.Error("Got nil but expected error", err)
				}
			}
		})
	}
}

func TestRelayInstallerCreateDir(t *testing.T) {
	cases := []struct {
		dir      string
		cleanup  bool
		expected error
	}{
		{"/etc/relay_test_dir", true, nil},
		{"/etc", false, errors.New("foo")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Create directory %s", c.dir), func(t *testing.T) {
			if c.cleanup {
				defer os.Remove(c.dir)
			}

			installer := RelayInstaller{
				Config: InstallConfig{
					InstallDir: c.dir,
				},
			}
			err := installer.CreateDir()

			if _, err := os.Stat(c.dir); os.IsNotExist(err) {
				t.Error("Expected the following directory to exist:", c.dir)
			}
			// In other words, if we expect to create a directory
			if c.expected == nil {
				if err != nil {
					t.Error("Expected nil, got", err)
				}
			} else {
				if err == nil {
					t.Error("Got nil but expected error", err)
				}
			}
		})
	}
}

func TestRelayInstallerCreateDirDryRun(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"

	defer os.RemoveAll(valid_dir)

	installer := RelayInstaller{
		Config: InstallConfig{
			InstallDir: valid_dir,
			DryRun:     true,
		},
	}

	err := installer.CreateDir()
	if err != nil {
		t.Error("CreateDir() should not result in error", err)
	}

	if _, err := os.Stat(valid_dir); !os.IsNotExist(err) {
		t.Error("The directory should not be created during a dry run")
	}
}

func TestRelayInstallerGenerateComposeConfig(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	cases := []struct {
		dir      string
		expected error
	}{
		{valid_dir, nil},
		{"/etc/non_existent_dir", errors.New("foo")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Saving config file to %s", c.dir), func(t *testing.T) {
			installer := RelayInstaller{
				Config: InstallConfig{
					InstallDir: c.dir,
				},
			}

			err := installer.GenerateComposeConfig()
			if c.expected == nil {
				if err != nil {
					t.Error("Expected nil, got", err)
				}
			} else {
				if err == nil {
					t.Error("Got nil but expected error", err)
				}
			}
		})
	}
}

func TestRelayInstallerGenerateComposeConfigYAML(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	uri := "https://example.invalid"
	token := "foobar"
	private_key := "mykey.pem"
	db := "mydb.store"
	user := "user"
	password := "password"
	hosts := []string{"host1", "host2"}
	fn := fmt.Sprintf("%s/relay.yml", valid_dir)

	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	installer := RelayInstaller{
		Config: InstallConfig{
			InstallDir: valid_dir,
		},
		Relay: Relay{
			Image: "my_relay_image",
			Config: RelayConfig{
				API_URI:                      uri,
				Organization_Token:           token,
				Private_Key:                  private_key,
				DB:                           db,
				Queue_User:                   user,
				Queue_Password:               password,
				Whitelisted_Repository_Hosts: hosts,
			},
		},
	}

	err := installer.GenerateComposeConfig()
	if err != nil {
		t.Error("Compose file generation should not result in an error", err)
	}

	if _, err := os.Stat(fn); os.IsNotExist(err) {
		t.Error("The Compose file does not exist")
	}

	contents, _ := ioutil.ReadFile(fn)
	config := ComposeConfig{}
	err = yaml.Unmarshal(contents, &config)

	if err != nil {
		t.Error("An error occurred while unmarshaling the test Compose file", err)
	}

	if config.Version != "3" {
		t.Error("Expected version 3, got", config.Version)
	}

	service := config.Services["relay"]

	if service.Restart != "always" {
		t.Error("Expected restart always, got", service.Restart)
	}

	if service.Container_Name != "norad_relay" {
		t.Error("Expected container name norad_relay, got", service.Container_Name)
	}

	if service.Image != "my_relay_image" {
		t.Error("Expected container name my_relay_image, got", service.Container_Name)
	}

	host_volume := fmt.Sprintf("%s:/relay:rw", valid_dir)
	if service.Volumes[0] != host_volume {
		t.Errorf("Expected volume %s, got %s", host_volume, service.Volumes[0])
	}

	if service.Volumes[1] != "/var/run/docker.sock:/relay/docker.sock:rw" {
		t.Errorf("Expected volume /var/run/docker.sock:/relay/docker.sock, got %s", service.Volumes[1])
	}

	if service.Environment[0] != "RELAY_MODE=production" {
		t.Error("Expected environment RELAY_MODE=production, got", service.Environment[0])
	}
}

func TestRelayInstallerGenerateComposeConfigDryRun(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	fn := fmt.Sprintf("%s/relay.yml", valid_dir)

	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	installer := RelayInstaller{
		Config: InstallConfig{
			InstallDir: valid_dir,
			DryRun:     true,
		},
	}

	err := installer.GenerateComposeConfig()
	if err != nil {
		t.Error("Compose file generation should not result in an error", err)
	}

	if _, err := os.Stat(fn); !os.IsNotExist(err) {
		t.Error("The Compose file should not be created during a dry run")
	}
}

func TestRelayInstallerGenerateRelayConfig(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	cases := []struct {
		dir      string
		expected error
	}{
		{valid_dir, nil},
		{"/etc/non_existent_dir", errors.New("foo")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Saving config file to %s", c.dir), func(t *testing.T) {
			installer := RelayInstaller{
				Config: InstallConfig{
					InstallDir: c.dir,
				},
			}

			err := installer.GenerateRelayConfig()
			if c.expected == nil {
				if err != nil {
					t.Error("Expected nil, got", err)
				}
			} else {
				if err == nil {
					t.Error("Got nil but expected error", err)
				}
			}
		})
	}
}

func TestRelayInstallerGenerateRelayConfigYAML(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	uri := "https://example.invalid"
	token := "foobar"
	private_key := "mykey.pem"
	db := "mydb.store"
	user := "user"
	password := "password"
	hosts := []string{"host1", "host2"}
	fn := fmt.Sprintf("%s/configuration.yml", valid_dir)

	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	installer := RelayInstaller{
		Config: InstallConfig{
			InstallDir: valid_dir,
		},
		Relay: Relay{
			Config: RelayConfig{
				API_URI:                      uri,
				Organization_Token:           token,
				Private_Key:                  private_key,
				DB:                           db,
				Queue_User:                   user,
				Queue_Password:               password,
				Whitelisted_Repository_Hosts: hosts,
			},
		},
	}

	err := installer.GenerateRelayConfig()
	if err != nil {
		t.Error("Relay configuration file generation should not result in an error", err)
	}

	if _, err := os.Stat(fn); os.IsNotExist(err) {
		fmt.Println(fn)
		t.Error("The Relay configuration file does not exist")
	}

	contents, _ := ioutil.ReadFile(fn)
	config := RelayConfig{}
	err = yaml.Unmarshal(contents, &config)

	if err != nil {
		t.Error("An error occurred while unmarshaling the test Relay configuration file", err)
	}

	if config.API_URI != uri {
		t.Errorf("Expected %s, got %s", uri, config.API_URI)
	}

	if config.Organization_Token != token {
		t.Errorf("Expected %s, got %s", token, config.Organization_Token)
	}

	if config.Private_Key != private_key {
		t.Errorf("Expected %s, got %s", private_key, config.Private_Key)
	}

	if config.DB != db {
		t.Errorf("Expected %s, got %s", db, config.DB)
	}

	if config.Queue_User != user {
		t.Errorf("Expected %s, got %s", user, config.Queue_User)
	}

	if config.Queue_Password != password {
		t.Errorf("Expected %s, got %s", password, config.Queue_Password)
	}

	for i, h := range hosts {
		if config.Whitelisted_Repository_Hosts[i] != h {
			t.Errorf("Expected %s, got %s", h, config.Whitelisted_Repository_Hosts[i])
		}
	}
}

func TestRelayInstallerGenerateRelayConfigDryRun(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	fn := fmt.Sprintf("%s/configuration.yml", valid_dir)

	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	installer := RelayInstaller{
		Config: InstallConfig{
			InstallDir: valid_dir,
			DryRun:     true,
		},
	}

	err := installer.GenerateRelayConfig()
	if err != nil {
		t.Error("Compose file generation should not result in an error", err)
	}

	if _, err := os.Stat(fn); !os.IsNotExist(err) {
		t.Error("The Compose file should not be created during a dry run")
	}
}

func TestRelayInstallerPullImageDryRun(t *testing.T) {
	installer := RelayInstaller{
		Config: InstallConfig{
			DryRun: true,
		},
		Relay: Relay{
			Image: "myimage",
		},
	}

	err := installer.PullImage()
	if err != nil {
		t.Error("A dry run of an image pull should not return an error", err)
	}
}

func TestRelayInstallerPullImage(t *testing.T) {
	installer := RelayInstaller{Relay: Relay{}}

	err := installer.PullImage()
	if err == nil {
		t.Error("A missing image name should result in an error, but no error returned")
	}

	cases := []struct {
		pull_error      error
		response_length int64 // to trigger an error
	}{
		{errors.New("1"), 0},
		{nil, 0},
		{nil, -1},
	}

	for _, c := range cases {
		test_resp := MockPullResp{"downloaded_image_bytes", c.response_length, -1}
		installer.Relay.Image = "foo"
		client := MockDockerClient{}
		client.On(
			"ImagePull",
			context.Background(),
			mock.MatchedBy(
				func(input string) bool {
					return input == "foo"
				},
			),
			mock.MatchedBy(
				func(input types.ImagePullOptions) bool {
					return input.All == false
				},
			),
		).Return(&test_resp, c.pull_error)
		installer.Docker = RelayDocker{Client: &client}
		ret := installer.PullImage()

		if c.pull_error != nil && ret == nil {
			t.Error("Expected image pull to return an error, but none occurred")
		} else if c.response_length == -1 && ret == nil {
			t.Error("Expected data read to result in error, but none occurred")
		}
		client.AssertNumberOfCalls(t, "ImagePull", 1)
	}
}

func TestRelayInstallerEnable(t *testing.T) {
	cases := []struct {
		dry_run     bool
		enable      bool
		expectation error
	}{
		{true, true, nil},
		{true, false, nil},
		{false, false, nil},
		{false, true, errors.New("")},
	}
	for _, c := range cases {
		installer := RelayInstaller{
			Config: InstallConfig{
				DryRun: c.dry_run,
				Enable: c.enable,
			},
		}
		err := installer.enable()
		if c.expectation == nil {
			if err != nil {
				t.Error("Expected nil, got", err)
			}
		} else {
			if err == nil {
				t.Errorf("Expected %s, got nil", err)
			}
		}
	}
}
