package relay

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/pkg/stdcopy"
)

type Logger interface {
	ReadLogs() (io.ReadCloser, error)
	Write([]byte) (int, error)
}

type RelayLogger struct {
	Docker        RelayDocker
	Follow        bool
	ContainerName string
}

func (l RelayLogger) Write(msg []byte) (int, error) {
	fmt.Printf("[Norad Relay Container Log] %s", msg)
	return len(msg), nil
}

func (l RelayLogger) ReadLogs() (io.ReadCloser, error) {
	log_opts := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Follow:     l.Follow,
	}

	return l.Docker.Client.ContainerLogs(context.Background(), l.ContainerName, log_opts)
}

func PrintLogs(logger Logger) error {
	resp, err := logger.ReadLogs()
	defer resp.Close()
	if err != nil {
		log.Printf("Unable to read the logs for the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}
	_, err = stdcopy.StdCopy(logger, os.Stderr, resp)
	return err
}
