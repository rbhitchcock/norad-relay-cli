package relay

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockSystemChecker struct {
	mock.Mock
}

func (c *MockSystemChecker) CheckDocker() error {
	args := c.Called()
	return args.Error(0)
}

func (c *MockSystemChecker) CheckApiConnectivity() error {
	args := c.Called()
	return args.Error(0)
}

func (c *MockSystemChecker) CheckRegistryConnectivity() error {
	args := c.Called()
	return args.Error(0)
}

func (c *MockSystemChecker) ProcessNetworkError(err error) {
}

func TestSystemCheck(t *testing.T) {
	cases := []struct {
		docker_conn   error
		api_conn      error
		registry_conn error
		expectation   error
	}{
		{errors.New("1"), nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, errors.New("2")},
		{nil, nil, errors.New("3"), errors.New("3")},
		{nil, nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution error detection for method %s", c.expectation), func(t *testing.T) {
			checker := new(MockSystemChecker)
			checker.On("CheckDocker").Return(c.docker_conn)
			checker.On("CheckApiConnectivity").Return(c.api_conn)
			checker.On("CheckRegistryConnectivity").Return(c.registry_conn)

			ret := SystemCheck(checker)

			if c.expectation != nil && ret == nil {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestSystemCheckerCheckApiConnectivity(t *testing.T) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}
	defer l.Close()

	cases := []struct {
		api_uri     string
		expectation error
	}{
		{l.Addr().String(), nil},
		{"invalid.host:1", errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing API connectivity checks for uri %s", c.api_uri), func(t *testing.T) {
			checker := RelaySystemChecker{
				ApiUri:      c.api_uri,
				ConnTimeout: (time.Duration(5) * time.Second),
			}

			ret := checker.CheckApiConnectivity()

			if c.expectation != nil && ret == nil {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestSystemCheckerCheckRegistryConnectivity(t *testing.T) {
	l1, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}
	l2, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}
	defer l1.Close()
	defer l2.Close()

	cases := []struct {
		registry_uri1 string
		registry_uri2 string
		expectation   error
	}{
		{l1.Addr().String(), l2.Addr().String(), nil},
		{l1.Addr().String(), "invalid.host:1", errors.New("1")},
		{"invalid.host:1", l2.Addr().String(), errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing API connectivity checks for URIs %s and %s", c.registry_uri1, c.registry_uri2), func(t *testing.T) {
			checker := RelaySystemChecker{
				RegistryUris: []string{c.registry_uri1, c.registry_uri2},
				ConnTimeout:  (time.Duration(5) * time.Second),
			}

			ret := checker.CheckRegistryConnectivity()

			if c.expectation != nil && ret == nil {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}

func TestSystemCheckerProcessNetworkError(t *testing.T) {
	cases := []struct {
		passed_error error
		expected_str string
		exact_match  bool
	}{
		{errors.New("foo"), "The raw error message is: foo", true},
		{&(net.OpError{Err: errors.New("opfoo")}), "DNS", false},
		{&(net.OpError{Addr: &(net.TCPAddr{}), Err: errors.New("timeout")}), "firewall settings", false},
	}
	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing API connectivity checks for error %s", c.passed_error), func(t *testing.T) {
			var str bytes.Buffer
			log.SetOutput(&str)

			checker := RelaySystemChecker{}
			checker.ProcessNetworkError(c.passed_error)

			if c.exact_match {
				if strings.TrimSpace(str.String()) != c.expected_str {
					t.Errorf("Expected '%s', but got '%s'", c.expected_str, strings.TrimSpace(str.String()))
				}
			} else {
				if !strings.Contains(strings.TrimSpace(str.String()), c.expected_str) {
					t.Errorf("Expected '%s' to contain '%s'", strings.TrimSpace(str.String()), c.expected_str)
				}
			}
		})
	}
}

func TestSystemCheckerCheckDocker(t *testing.T) {
	cases := []struct {
		expected_error error
	}{
		{nil},
		{errors.New("1")},
	}

	for _, c := range cases {
		checker := RelaySystemChecker{}
		client := MockDockerClient{}
		client.On(
			"ServerVersion",
			context.Background(),
		).Return(types.Version{}, c.expected_error)
		checker.Docker = RelayDocker{Client: &client}
		ret := checker.CheckDocker()
		if c.expected_error == nil && ret != nil {
			t.Errorf("No error was expected, but %s was returned", ret)
		} else if c.expected_error != nil && ret == nil {
			t.Errorf("An error was expected, but none was returned")
		}
	}
}
