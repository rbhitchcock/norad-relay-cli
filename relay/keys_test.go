package relay

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestRelaySigningKeyInitializeKey(t *testing.T) {
	valid_path := "/etc/relay_test_dir"
	dne_path := "/etc/relay_test_dir_dne"
	os.Mkdir(valid_path, 0700)
	defer os.RemoveAll(valid_path)

	dne_key := fmt.Sprintf("%s/relay.pem", dne_path)
	invalid_key := fmt.Sprintf("%s/invalid.pem", valid_path)
	public_key := fmt.Sprintf("%s/public.pem", valid_path)
	valid_key := fmt.Sprintf("%s/valid.pem", valid_path)

	keyGeneratorForKeyTests(public_key, false, true)
	keyGeneratorForKeyTests(valid_key, true, false)
	invalid_key_fh, _ := os.Create(invalid_key)
	defer invalid_key_fh.Close()
	invalid_key_fh.Write([]byte{41})

	cases := []struct {
		key_path       string
		expected_error error
	}{
		{expected_error: errors.New("No path to key has been specified.")},
		{key_path: dne_key, expected_error: errors.New("no such file or directory")},
		{key_path: invalid_key, expected_error: errors.New("Unable to load key")},
		{key_path: public_key, expected_error: errors.New("integer too large")},
		{key_path: valid_key},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing key initialization for %s", c.key_path), func(t *testing.T) {
			key := &RelaySigningKey{File: c.key_path}
			err := key.InitializeKey()
			if c.expected_error != nil {
				if err == nil || !strings.Contains(err.Error(), c.expected_error.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expected_error, err)
				}
			} else {
				if key.Privkey == nil {
					t.Error("The key should not be nil if no errors were raised.")
				}
			}
		})
	}
}

func TestRelaySigningKeyFingerprint(t *testing.T) {
	valid_path := "/etc/relay_test_dir"
	os.Mkdir(valid_path, 0700)
	defer os.RemoveAll(valid_path)

	// privkey nil, initialize fails
	dne_key := "/etc/relay_test_dir_dne/relay.pem"

	// privkey nil, initialize succeeds
	valid_key := fmt.Sprintf("%s/valid.pem", valid_path)
	keyGeneratorForKeyTests(valid_key, true, false)

	// modify public key somehow
	brokenkey := keyGeneratorForKeyTests("", false, false)
	brokenkey.PublicKey.N = nil

	// success
	privkey := keyGeneratorForKeyTests("", false, false)

	cases := []struct {
		key_path       string
		private_key    *rsa.PrivateKey
		expected_error error
	}{
		{key_path: dne_key, expected_error: errors.New("no such file or directory")},
		{key_path: valid_key},
		{private_key: brokenkey, expected_error: errors.New("1")},
		{private_key: privkey},
	}

	for _, c := range cases {
		key := &RelaySigningKey{File: c.key_path, Privkey: c.private_key}
		fp, err := key.Fingerprint()

		if c.expected_error != nil {
			if err == nil || !strings.Contains(err.Error(), c.expected_error.Error()) {
				t.Errorf("Expected error %s, but got %s", c.expected_error, err)
			}
		} else {
			if len(fp) < sha256.Size {
				t.Errorf("Fingerprint is not the correct length: %X", fp)
			}
		}
	}
}

func keyGeneratorForKeyTests(keypath string, save_private bool, save_public bool) *rsa.PrivateKey {
	key, _ := rsa.GenerateKey(rand.Reader, 2048)
	var file *os.File
	if save_private || save_public {
		file, _ = os.Create(keypath)
		defer file.Close()
	}

	if save_private {
		pem.Encode(
			file,
			&pem.Block{
				Type:  "PRIVATE KEY",
				Bytes: x509.MarshalPKCS1PrivateKey(key),
			},
		)
	} else if save_public {
		bytes, _ := asn1.Marshal(key.PublicKey)
		pem.Encode(
			file,
			&pem.Block{
				Type:  "PUBLIC KEY",
				Bytes: bytes,
			},
		)
	}
	return key
}
