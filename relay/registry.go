package relay

import (
	manifest "github.com/docker/distribution/manifest/schema2"
)

type RelayRegistry interface {
	ManifestV2(string, string) (*manifest.DeserializedManifest, error)
}
