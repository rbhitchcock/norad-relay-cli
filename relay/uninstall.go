package relay

import (
	"context"
	"log"
	"os"

	"github.com/docker/docker/api/types"
)

type Uninstaller interface {
	Disable() error
	RemoveImage() error
	RemoveConfigDir() error
}

type RelayUninstaller struct {
	Disabler RelayDisabler
	Config   UninstallConfig
	Relay    Relay
}

type UninstallConfig struct {
	Dir string
}

func (uninstaller RelayUninstaller) Disable() error {
	return Disable(uninstaller.Disabler)
}

func (uninstaller RelayUninstaller) RemoveImage() error {
	log.Printf("Removing image %s...", uninstaller.Relay.Image)

	_, err := uninstaller.Disabler.Docker.Client.ImageRemove(
		context.Background(),
		uninstaller.Relay.Image,
		types.ImageRemoveOptions{Force: true, PruneChildren: true},
	)
	if err != nil {
		log.Printf("Something went wrong while removing the image: %s.", err)
		return err
	}

	log.Print("Image successfully removed.")

	return nil
}

func (uninstaller RelayUninstaller) RemoveConfigDir() error {
	dir := uninstaller.Config.Dir
	log.Printf("Removing %s...", dir)
	err := os.RemoveAll(dir)

	if err != nil {
		log.Printf("An error occurred while removing directory %s.", dir)
		log.Print(err)
	} else {
		log.Printf("Successfully removed the directory %s.", dir)
	}
	return err
}

func Uninstall(uninstaller Uninstaller) error {
	err := uninstaller.Disable()
	if err != nil {
		log.Printf("Something went wrong while disabling the Relay.")
		log.Printf("Stopping uninstallation. The configuration directory still exists.")
		return err
	}

	err = uninstaller.RemoveImage()
	if err != nil {
		log.Printf("Stopping uninstallation. The configuration directory still exists.")
		return err
	}

	err = uninstaller.RemoveConfigDir()
	if err != nil {
		log.Printf("Stopping uninstallation. The configuration directory still exists.")
		return err
	}

	return nil
}
