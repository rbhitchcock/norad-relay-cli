package relay

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gopkg.in/yaml.v2"
)

type MockFixer struct {
	mock.Mock
}

func (f *MockFixer) LoadMalformedConfig() (ComposeService, error) {
	args := f.Called()
	return args.Get(0).(ComposeService), args.Error(1)
}

func (f *MockFixer) WriteFixedConfig(service ComposeService) error {
	args := f.Called(service)
	return args.Error(0)
}

func TestFixupConfig(t *testing.T) {
	cases := []struct {
		load_container_config error
		load_malformed_config error
		write_fixed_config    error
		expectation           error
	}{
		{
			load_container_config: errors.New("Outdated Relay file."),
			load_malformed_config: errors.New("1"),
			expectation:           errors.New("1"),
		},
		{
			load_container_config: errors.New("Outdated Relay file."),
			write_fixed_config:    errors.New("2"),
			expectation:           errors.New("2"),
		},
		{
			load_container_config: errors.New("0"),
			expectation:           errors.New("0"),
		},
		{
			load_container_config: nil,
			expectation:           nil,
		},
		{
			load_container_config: errors.New("Outdated Relay file."),
			expectation:           nil,
		},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			enabler := new(MockEnabler)
			enabler.On("LoadContainerConfig").Return(ComposeService{}, c.load_container_config)
			testObj := new(MockFixer)
			testObj.On("LoadMalformedConfig").Return(ComposeService{}, c.load_malformed_config)
			testObj.On("WriteFixedConfig", ComposeService{}).Return(c.write_fixed_config)
			ret := FixupConfig(testObj, enabler)
			if c.expectation == nil {
				if ret != nil {
					t.Errorf("Expected nil, got %s", ret)
				}
			} else {
				if ret == nil || (ret.Error() != c.expectation.Error()) {
					t.Errorf("Expected Error %s, got %s", c.expectation, ret)
				}
			}
		})
	}
}

func TestRelayLoadMalformedConfig(t *testing.T) {
	dne_dir := "/etc/dne_relay_test_dir"
	valid_dir := "/etc/valid_relay_test_dir"
	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	logging_config := RelayLogConfig{
		Driver: "json-file",
		Options: map[string]string{
			"max-size": "20m",
			"max-file": "5",
		},
	}
	invalid_service := ComposeService{}
	no_logging_service := ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Environment:    []string{"foo=bar"},
	}
	no_env_service := ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Logging:        logging_config,
	}
	valid_service := ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Environment:    []string{"foo=bar"},
		Logging:        logging_config,
	}

	compose_file_path := fmt.Sprintf("%s/relay.yml", valid_dir)

	cases := []struct {
		dir            string
		dne            bool
		service        ComposeService
		invalid_data   []byte
		expected_error error
	}{
		{dir: dne_dir, dne: true, expected_error: errors.New("open /etc/dne_relay_test_dir/relay.yml: no such file or directory")},
		{dir: valid_dir, service: invalid_service, expected_error: errors.New("File is missing required fields.")},
		{dir: valid_dir, invalid_data: []byte{41}, expected_error: errors.New("yaml: unmarshal errors")},
		{dir: valid_dir, service: no_logging_service},
		{dir: valid_dir, service: no_env_service},
		{dir: valid_dir, service: valid_service},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Loading outdated config for %s", c.service), func(t *testing.T) {
			if !c.dne {
				if len(c.invalid_data) == 0 {
					service_map := make(map[string]ComposeService)
					service_map["relay"] = c.service
					d, _ := yaml.Marshal(&service_map)
					ioutil.WriteFile(compose_file_path, d, 0600)
				} else {
					ioutil.WriteFile(compose_file_path, c.invalid_data, 0600)
				}
			}

			fixer := RelayConfigFixer{ConfigDir: c.dir}
			loaded_config, err := fixer.LoadMalformedConfig()

			if c.expected_error != nil {
				if !strings.Contains(err.Error(), c.expected_error.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expected_error, err)
				}
			} else {
				if loaded_config.Container_Name == "" {
					t.Errorf("Loaded wrong container name: %s", loaded_config.Container_Name)
				}

				if loaded_config.Container_Name != c.service.Container_Name {
					t.Errorf("Loaded wrong container name: %s", loaded_config.Container_Name)
				}

				if loaded_config.Restart != c.service.Restart {
					t.Errorf("Loaded wrong restart policy: %s", loaded_config.Restart)
				}

				if loaded_config.Image != c.service.Image {
					t.Errorf("Loaded wrong image name: %s", loaded_config.Image)
				}

				for i, _ := range c.service.Volumes {
					if loaded_config.Volumes[i] != c.service.Volumes[i] {
						t.Errorf("Loaded wrong volume: %s", loaded_config.Volumes[i])
					}
				}

				for i, _ := range c.service.Environment {
					if loaded_config.Environment[i] != c.service.Environment[i] {
						t.Errorf("Loaded wrong environment: %s", loaded_config.Environment[i])
					}
				}
			}
		})
	}
}

func TestRelayWriteFixedConfig(t *testing.T) {
	logging_config := RelayLogConfig{
		Driver: "json-file",
		Options: map[string]string{
			"max-size": "20m",
			"max-file": "5",
		},
	}
	no_logging_service := ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Environment:    []string{"foo=bar"},
	}
	no_env_service := ComposeService{
		Restart:        "never",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Logging:        logging_config,
	}
	valid_service := ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
		Volumes:        []string{"a:b", "c:d"},
		Environment:    []string{"foo=bar"},
		Logging:        logging_config,
	}

	valid_dir := "/etc/relay_test_dir"
	os.Mkdir(valid_dir, 0700)
	defer os.RemoveAll(valid_dir)

	cases := []struct {
		dir            string
		service        ComposeService
		expected_error error
	}{
		{
			dir:     valid_dir,
			service: valid_service,
		},
		{
			dir:     valid_dir,
			service: no_logging_service,
		},
		{
			dir:     valid_dir,
			service: no_env_service,
		},
		{
			dir:            "/etc/non_existent_dir",
			service:        valid_service,
			expected_error: errors.New("no such file or directory"),
		},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Saving config file to %s", c.dir), func(t *testing.T) {
			fixer := RelayConfigFixer{ConfigDir: c.dir}

			err := fixer.WriteFixedConfig(c.service)

			if c.expected_error != nil {
				if !strings.Contains(err.Error(), c.expected_error.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expected_error, err)
				}
			} else {
				fn := fmt.Sprintf("%s/relay.yml", c.dir)
				contents, _ := ioutil.ReadFile(fn)
				config := ComposeConfig{}
				err = yaml.Unmarshal(contents, &config)
				if err != nil {
					t.Errorf("An unexpected error occurred: %s", err)
				}
				loaded_service := config.Services["relay"]
				if loaded_service.Container_Name == "" {
					t.Errorf("Wrote wrong container name: %s", loaded_service.Container_Name)
				}

				if loaded_service.Container_Name != c.service.Container_Name {
					t.Errorf("Wrote wrong container name: %s", loaded_service.Container_Name)
				}

				if loaded_service.Restart != "always" {
					t.Errorf("Wrote wrong restart policy: %s", loaded_service.Restart)
				}

				if loaded_service.Image != c.service.Image {
					t.Errorf("Wrote wrong image name: %s", loaded_service.Image)
				}

				for i, _ := range c.service.Volumes {
					if loaded_service.Volumes[i] != c.service.Volumes[i] {
						t.Errorf("Wrote wrong volume: %s", loaded_service.Volumes[i])
					}
				}

				for i, _ := range c.service.Environment {
					if loaded_service.Environment[i] != c.service.Environment[i] {
						t.Errorf("Wrote wrong environment: %s", loaded_service.Environment[i])
					}
				}

				if loaded_service.Logging.Driver != logging_config.Driver {
					t.Errorf("Logging driver is incorrect: %s", loaded_service.Logging.Driver)
				}
			}
		})
	}
}
