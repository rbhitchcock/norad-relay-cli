package relay

type Relay struct {
	Image  string
	Config RelayConfig
}

type RelayConfig struct {
	API_URI                      string
	Organization_Token           string
	Private_Key                  string
	DB                           string
	Queue_User                   string
	Queue_Password               string
	Whitelisted_Repository_Hosts []string
}
