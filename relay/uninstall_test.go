package relay

import (
	"context"
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/stretchr/testify/mock"
)

type MockUninstaller struct {
	mock.Mock
}

func (d *MockUninstaller) Disable() error {
	args := d.Called()
	return args.Error(0)
}

func (d *MockUninstaller) RemoveImage() error {
	args := d.Called()
	return args.Error(0)
}

func (d *MockUninstaller) RemoveConfigDir() error {
	args := d.Called()
	return args.Error(0)
}

func TestUninstall(t *testing.T) {
	cases := []struct {
		disable           error
		remove_image      error
		remove_config_dir error
		expectation       error
	}{
		{errors.New("1"), nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, errors.New("2")},
		{nil, nil, errors.New("3"), errors.New("3")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockUninstaller)
			testObj.On("Disable").Return(c.disable)
			testObj.On("RemoveImage").Return(c.remove_image)
			testObj.On("RemoveConfigDir").Return(c.remove_config_dir)
			ret := Uninstall(testObj)

			if ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			}
		})
	}
}

func TestRelayUninstallerRemoveConfigDir(t *testing.T) {
	test_dir := "/etc/relay_test_dir"
	os.Mkdir(test_dir, 0700)
	defer os.RemoveAll(test_dir)

	uninstaller := RelayUninstaller{
		Config: UninstallConfig{
			Dir: test_dir,
		},
	}
	uninstaller.RemoveConfigDir()
	if _, err := os.Stat(test_dir); !os.IsNotExist(err) {
		t.Error("Expected the following directory to not exist:", test_dir)
	}
}

func TestRelayUninstallerRemoveImage(t *testing.T) {

	cases := []struct {
		image_name     string
		expected_error error
	}{
		{"foo", nil},
		{"bar", errors.New("bar")},
	}

	for _, c := range cases {
		uninstaller := RelayUninstaller{Relay: Relay{Image: c.image_name}, Disabler: RelayDisabler{}}
		client := MockDockerClient{}
		client.On(
			"ImageRemove",
			context.Background(),
			c.image_name,
			types.ImageRemoveOptions{Force: true, PruneChildren: true},
		).Return([]types.ImageDeleteResponseItem{}, c.expected_error)
		uninstaller.Disabler.Docker = RelayDocker{Client: &client}
		ret := uninstaller.RemoveImage()

		if c.expected_error == nil && ret != nil {
			t.Errorf("Expected image remove to retun nil, but %s occurred", ret)
		} else if c.expected_error != nil && ret == nil {
			t.Error("Expected image remove to return an error, but none occurred")
		}
		client.AssertNumberOfCalls(t, "ImageRemove", 1)
	}
}
