package relay

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/hashicorp/go-version"
	"gopkg.in/yaml.v2"
)

type Installer interface {
	ValidateConfig() error
	ValidateVersion() error
	PullImage() error
	CreateDir() error
	GenerateComposeConfig() error
	GenerateRelayConfig() error
	enable() error
}

type RelayInstaller struct {
	Docker RelayDocker
	Config InstallConfig
	Relay  Relay
}

type InstallConfig struct {
	InstallDir string
	DryRun     bool
	Enable     bool
}

func (installer RelayInstaller) PullImage() error {
	if installer.Config.DryRun {
		log.Printf("User has enabled a dry run. Relay image %s will not be be downloaded.", installer.Relay.Image)
		return nil
	}

	if installer.Relay.Image == "" {
		log.Printf("No image specified for download.")
		return errors.New("No image specified for download.")
	}

	log.Printf("Downloading image %s... (this may take a while)", installer.Relay.Image)

	resp, err := installer.Docker.Client.ImagePull(context.Background(), installer.Relay.Image, types.ImagePullOptions{})
	if err != nil {
		log.Printf("Something went wrong while downloading the image: %s.", err)
		return err
	}
	_, err = ioutil.ReadAll(resp)
	if err != nil {
		log.Printf("Something went wrong while downloading the image: %s.", err)
		return err
	}
	log.Print("Image successfully downloaded.")

	return err
}

// ValidateConfig checks to make sure that all of the prerequisites have been met for Relay
// installation and that the configuration is valid.
func (installer RelayInstaller) ValidateConfig() error {
	log.Printf("Validating install %s directory does not exist...", installer.Config.InstallDir)
	_, err := os.Stat(installer.Config.InstallDir)

	if !os.IsNotExist(err) {
		log.Printf("The installation directory %s already exists. Please remove it and retry intallation.", installer.Config.InstallDir)
		if !installer.Config.DryRun {
			return &relayDirExistsError{installer.Config.InstallDir}
		}

	} else {
		log.Print("OK")
	}

	return nil
}

func (installer RelayInstaller) ValidateVersion() error {
	log.Print("Checking Docker version...")
	min_ver, _ := version.NewVersion(installer.Docker.MinVer)
	server_ver, _ := version.NewVersion(installer.Docker.ServerVer)

	if server_ver.LessThan(min_ver) {
		log.Printf("Version %s is not supported. Install Docker version %s or newer.", server_ver, min_ver)
		return &unsupportedDockerVersionError{min_ver.String(), server_ver.String()}
	}
	log.Printf("Version %s is supported.", server_ver)

	return nil
}

func (installer RelayInstaller) CreateDir() error {
	dir := installer.Config.InstallDir
	if installer.Config.DryRun {
		log.Printf("User has enabled a dry run. Directory %s will not be created.", dir)
		return nil
	}
	err := os.Mkdir(dir, 0700)
	if err != nil {
		log.Printf("An error occurred while creating directory %s.", dir)
		log.Print(err)
	} else {
		log.Printf("Successfully created the directory %s.", dir)
	}
	return err
}

func (installer RelayInstaller) GenerateComposeConfig() error {
	service := make(map[string]ComposeService)
	fn := fmt.Sprintf("%s/relay.yml", installer.Config.InstallDir)
	config_volume := fmt.Sprintf("%s:/relay:rw", installer.Config.InstallDir)
	log_config := RelayLogConfig{
		Driver: "json-file",
		Options: map[string]string{
			"max-size": "20m",
			"max-file": "5",
		},
	}

	log.Printf("Generating Docker Compose file...")
	service["relay"] = ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          installer.Relay.Image,
		Volumes:        []string{config_volume, "/var/run/docker.sock:/relay/docker.sock:rw"},
		Environment:    []string{"RELAY_MODE=production"},
		Logging:        log_config,
	}
	config := ComposeConfig{
		Version:  "3",
		Services: service,
	}

	d, err := yaml.Marshal(&config)
	if err != nil {
		log.Print("Unable to generate YAML for Compose file.")
		return err
	}

	if installer.Config.DryRun {
		log.Printf("User has enabled a dry run. The following Docker Compose file will not be written to %s:\n%s", fn, d)
		return nil
	}

	err = ioutil.WriteFile(fn, d, 0600)
	if err != nil {
		log.Printf("Unable to write to Docker Compose file %s.", fn)
		return err
	}

	log.Printf("Docker Compose file successfully written to %s.", fn)
	return err
}

func (installer RelayInstaller) GenerateRelayConfig() error {
	fn := fmt.Sprintf("%s/configuration.yml", installer.Config.InstallDir)
	log.Printf("Generating Relay configuration file...")

	d, err := yaml.Marshal(&installer.Relay.Config)
	if err != nil {
		log.Print("Unable to generate YAML for Relay configuration file.")
		return err
	}
	if installer.Config.DryRun {
		log.Printf("User has enabled a dry run. The following Relay configuration file will not be written to %s:\n%s", fn, d)
		return nil
	}

	err = ioutil.WriteFile(fn, d, 0600)
	if err != nil {
		log.Printf("Unable to write to Relay configuration file %s.", fn)
		return err
	}

	log.Printf("Relay configuration file successfully written to %s.", fn)
	return err
}

func (installer RelayInstaller) enable() error {
	if installer.Config.DryRun || !installer.Config.Enable {
		log.Printf("Enable after install is disabled.")
		return nil
	}

	log.Printf("Enabling the Norad Relay.")
	enabler := RelayEnabler{
		Config: EnableConfig{
			ConfigDir: installer.Config.InstallDir,
			Force:     false,
		},
		Relay:  installer.Relay,
		Docker: installer.Docker,
	}
	return Enable(enabler)
}

func Install(installer Installer) error {
	err := installer.ValidateConfig()
	if err != nil {
		return err
	}

	err = installer.ValidateVersion()
	if err != nil {
		return err
	}

	err = installer.PullImage()
	if err != nil {
		return err
	}

	err = installer.CreateDir()
	if err != nil {
		return err
	}

	err = installer.GenerateComposeConfig()
	if err != nil {
		return err
	}

	err = installer.GenerateRelayConfig()
	if err != nil {
		return err
	}

	err = installer.enable()
	if err != nil {
		return err
	}

	return nil
}

type relayDirExistsError struct {
	dir string
}

func (e *relayDirExistsError) Error() string {
	return fmt.Sprintf("Install directory %s already exists.", e.dir)
}

type unsupportedDockerVersionError struct {
	min_ver    string
	actual_ver string
}

func (e *unsupportedDockerVersionError) Error() string {
	return fmt.Sprintf("The reported version of %s is less than the minimum supported_version %s", e.min_ver, e.actual_ver)
}
