package relay

import (
	"errors"
	"fmt"
	"testing"
)

func TestRestart(t *testing.T) {
	cases := []struct {
		enabler     error
		disabler    error
		expectation error
	}{
		{errors.New("1"), nil, errors.New("1")},
		{nil, errors.New("2"), errors.New("2")},
		{nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			disabler := new(MockDisabler)
			disabler.On("FindContainer").Return("foo", c.disabler)
			disabler.On("StopContainer").Return(nil)
			disabler.On("RemoveContainer").Return(nil)

			enabler := new(MockEnabler)
			enabler.On("ValidateInstall").Return(c.enabler)
			enabler.On("CheckForExisting").Return(nil)
			enabler.On("LoadContainerConfig").Return(ComposeService{}, nil)
			enabler.On("CreateContainer").Return("foo", nil)
			enabler.On("StartContainer").Return(nil)

			ret := Restart(enabler, disabler)

			if c.expectation != nil && ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			} else if c.expectation == nil && ret != nil {
				t.Errorf("Expected no error, got %s", ret)
			}
		})
	}
}
