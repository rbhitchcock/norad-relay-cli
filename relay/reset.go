package relay

import (
	"log"
)

func Reset(installer Installer, uninstaller Uninstaller, disabler Disabler) error {
	err := Disable(disabler)
	if err != nil {
		log.Printf("Unable to stop the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	err = Uninstall(uninstaller)
	if err != nil {
		log.Printf("Unable to uninstall the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	err = Install(installer)
	if err != nil {
		log.Printf("Unable to install the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	log.Printf("Relay reset!")
	return nil
}
