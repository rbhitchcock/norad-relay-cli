package relay

import (
	"context"
	"log"
	"time"

	"github.com/docker/docker/api/types"
)

type Disabler interface {
	FindContainer() (string, error)
	StopContainer(string) error
	RemoveContainer(string) error
}

type RelayDisabler struct {
	Docker RelayDocker
}

func (relay_disabler RelayDisabler) FindContainer() (string, error) {
	return findRelayContainer(relay_disabler.Docker.Client, false)
}

func (relay_disabler RelayDisabler) StopContainer(cid string) error {
	log.Print("Stopping the Norad Relay container...")
	timeout := time.Duration(60) * time.Second
	err := relay_disabler.Docker.Client.ContainerStop(context.Background(), cid, &timeout)
	if err == nil {
		log.Print("OK")
	}
	return err
}

func (relay_disabler RelayDisabler) RemoveContainer(cid string) error {
	log.Print("Removing the Norad Relay container...")
	err := relay_disabler.Docker.Client.ContainerRemove(
		context.Background(),
		cid,
		types.ContainerRemoveOptions{RemoveVolumes: true, Force: true},
	)
	if err == nil {
		log.Print("OK")
	}
	return err
}

func Disable(disabler Disabler) error {
	container_id, err := disabler.FindContainer()
	if err != nil {
		log.Print("An error occurred while searching for the Norad Relay container.")
		log.Print(err)
		return err
	}
	if container_id == "" {
		log.Print("No Norad Relay container exists to disable.")
		return nil
	}

	err = disabler.StopContainer(container_id)
	if err != nil {
		log.Printf("Unable to stop the container %s.", container_id)
		log.Printf("The following error occurred: %s", err)
		return err
	}

	err = disabler.RemoveContainer(container_id)
	if err != nil {
		log.Printf("Unable to remove the container %s.", container_id)
		log.Printf("The following error occurred: %s", err)
		return err
	}

	return nil
}
