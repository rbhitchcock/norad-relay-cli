package relay

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/stretchr/testify/mock"
	"gopkg.in/yaml.v2"
)

type MockEnabler struct {
	mock.Mock
}

func (e *MockEnabler) ValidateInstall() error {
	args := e.Called()
	return args.Error(0)
}

func (e *MockEnabler) CheckForExisting() error {
	args := e.Called()
	return args.Error(0)
}

func (e *MockEnabler) LoadContainerConfig() (ComposeService, error) {
	args := e.Called()
	return args.Get(0).(ComposeService), args.Error(1)
}

func (e *MockEnabler) CreateContainer(service ComposeService) (string, error) {
	args := e.Called()
	return args.String(0), args.Error(1)
}

func (e *MockEnabler) StartContainer(id string) error {
	args := e.Called()
	return args.Error(0)
}

func (e *MockEnabler) Disable(id string) error {
	args := e.Called()
	return args.Error(0)
}

func TestEnable(t *testing.T) {
	cases := []struct {
		validate_install      error
		check_for_existing    error
		load_container_config error
		create_container      error
		start_container       error
		expectation           error
	}{
		{errors.New("1"), nil, nil, nil, nil, errors.New("1")},
		{nil, errors.New("2"), nil, nil, nil, errors.New("2")},
		{nil, nil, errors.New("3"), nil, nil, errors.New("3")},
		{nil, nil, nil, errors.New("4"), nil, errors.New("4")},
		{nil, nil, nil, nil, errors.New("5"), errors.New("5")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockEnabler)
			testObj.On("ValidateInstall").Return(c.validate_install)
			testObj.On("CheckForExisting").Return(c.check_for_existing)
			testObj.On("LoadContainerConfig").Return(ComposeService{}, c.load_container_config)
			testObj.On("CreateContainer").Return("foo", c.create_container)
			testObj.On("StartContainer").Return(c.start_container)
			ret := Enable(testObj)
			if ret.Error() != c.expectation.Error() {
				t.Errorf("Expected Error %s, got %s", c.expectation, ret)
			}
		})
	}
}

func TestRelayEnablerValidateInstall(t *testing.T) {
	valid_dir := "/etc/relay_test_dir"
	no_compose_dir := "/etc/no_compose_relay_test_dir"
	no_config_dir := "/etc/no_config_relay_test_dir"
	invalid_config_dir := "/etc/invalid_config_relay_test_dir"

	os.Mkdir(valid_dir, 0700)
	os.Mkdir(no_compose_dir, 0700)
	os.Mkdir(no_config_dir, 0700)
	os.Mkdir(invalid_config_dir, 0700)

	defer os.RemoveAll(valid_dir)
	defer os.RemoveAll(no_compose_dir)
	defer os.RemoveAll(no_config_dir)
	defer os.RemoveAll(invalid_config_dir)

	compose1 := fmt.Sprintf("%s/relay.yml", valid_dir)
	compose2 := fmt.Sprintf("%s/relay.yml", no_config_dir)
	compose3 := fmt.Sprintf("%s/relay.yml", invalid_config_dir)
	ioutil.WriteFile(compose1, [](byte)("foo\n"), 0600)
	ioutil.WriteFile(compose2, [](byte)("foo\n"), 0600)
	ioutil.WriteFile(compose3, [](byte)("foo\n"), 0600)

	config1 := fmt.Sprintf("%s/configuration.yml", valid_dir)
	config2 := fmt.Sprintf("%s/configuration.yml", no_compose_dir)
	config3 := fmt.Sprintf("%s/configuration.yml", invalid_config_dir)
	ioutil.WriteFile(config1, [](byte)("organization_token: 1234\n"), 0600)
	ioutil.WriteFile(config2, [](byte)("foo: bar"), 0600)
	ioutil.WriteFile(config3, [](byte)("foo: bar"), 0600)

	cases := []struct {
		dir      string
		expected error
	}{
		{valid_dir, nil},
		{no_compose_dir, errors.New("stat /etc/no_compose_relay_test_dir/relay.yml: no such file or directory")},
		{no_config_dir, errors.New("stat /etc/no_config_relay_test_dir/configuration.yml: no such file or directory")},
		{invalid_config_dir, errors.New("Organization token is missing from configuration file.")},
		{"/nonexistent", errors.New("stat /nonexistent: no such file or directory")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Validating install to %s", c.dir), func(t *testing.T) {
			enabler := RelayEnabler{
				Config: EnableConfig{
					ConfigDir: c.dir,
				},
			}
			ret := enabler.ValidateInstall()

			if c.expected == nil {
				if ret != nil {
					t.Error("Expected nil, got", ret)
				}
			} else {
				if c.expected.Error() != ret.Error() {
					t.Errorf("Expected %s, got %s", c.expected, ret)
				}
			}
		})
	}
}

func TestRelayLoadContainerConfig(t *testing.T) {
	// file dne
	// file is invalid
	// file is valid
	dne_dir := "/etc/dne_relay_test_dir"
	invalid_dir := "/etc/invalid_relay_test_dir"
	valid_dir := "/etc/valid_relay_test_dir"
	unsupported_dir := "/etc/unsupported_relay_test_dir"

	os.Mkdir(dne_dir, 0700)
	os.Mkdir(invalid_dir, 0700)
	os.Mkdir(valid_dir, 0700)
	os.Mkdir(unsupported_dir, 0700)

	defer os.RemoveAll(valid_dir)
	defer os.RemoveAll(unsupported_dir)
	defer os.RemoveAll(dne_dir)
	defer os.RemoveAll(invalid_dir)

	compose_valid := fmt.Sprintf("%s/relay.yml", valid_dir)
	compose_unsupported := fmt.Sprintf("%s/relay.yml", unsupported_dir)
	compose_invalid := fmt.Sprintf("%s/relay.yml", invalid_dir)

	service := make(map[string]ComposeService)
	service["relay"] = ComposeService{
		Restart:        "always",
		Container_Name: "norad_relay",
		Image:          "foo",
	}
	config := ComposeConfig{
		Version:  "3",
		Services: service,
	}

	d, _ := yaml.Marshal(&config)

	ioutil.WriteFile(compose_valid, d, 0600)
	ioutil.WriteFile(compose_invalid, [](byte)("foobar\n"), 0600)
	ioutil.WriteFile(compose_unsupported, [](byte)("foo: bar\n"), 0600)

	cases := []struct {
		dir      string
		expected error
	}{
		{valid_dir, nil},
		{invalid_dir, errors.New("yaml: unmarshal errors")},
		{dne_dir, errors.New("no such file or directory")},
		{unsupported_dir, errors.New("Outdated Relay file.")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Validating install to %s", c.dir), func(t *testing.T) {
			enabler := RelayEnabler{
				Config: EnableConfig{
					ConfigDir: c.dir,
				},
			}
			_, ret := enabler.LoadContainerConfig()

			if c.expected == nil {
				if ret != nil {
					t.Error("Expected nil, got", ret)
				}
			} else {
				if !strings.Contains(ret.Error(), c.expected.Error()) {
					t.Errorf("Expected %s, got %s", c.expected, ret)
				}
			}
		})
	}
}

func TestRelayEnablerCreateContainer(t *testing.T) {
	cases := []struct {
		restart        string
		container_name string
		image          string
		volumes        []string
		environment    []string
		log_driver     string
		expected_error error
	}{
		{"yes", "foo1", "bar1", []string{"a:b", "c:d"}, []string{"mode=prod"}, "json", nil},
		{"no", "foo2", "bar2", []string{"b:a", "d:c"}, []string{"mode=debug"}, "awslogs", errors.New("1")},
	}

	for _, c := range cases {
		log_config := RelayLogConfig{Driver: c.log_driver, Options: make(map[string]string, 1)}
		service := ComposeService{
			Restart:        c.restart,
			Container_Name: c.container_name,
			Image:          c.image,
			Volumes:        c.volumes,
			Environment:    c.environment,
			Logging:        log_config,
		}
		client := MockDockerClient{}
		client.On(
			"ContainerCreate",
			context.Background(),
			mock.MatchedBy(
				func(input *container.Config) bool {
					return input.Env[0] == c.environment[0] && input.Image == c.image
				},
			),
			mock.MatchedBy(
				func(input *container.HostConfig) bool {
					return input.RestartPolicy.Name == c.restart && input.LogConfig.Type == c.log_driver
				},
			),
			&network.NetworkingConfig{},
			mock.MatchedBy(
				func(input string) bool {
					return input == c.container_name
				},
			),
		).Return(container.ContainerCreateCreatedBody{ID: "foo"}, c.expected_error)
		enabler := RelayEnabler{
			Docker: RelayDocker{Client: &client},
		}
		id, err := enabler.CreateContainer(service)

		if c.expected_error == nil && err != nil {
			t.Errorf("Expected no error, but received %s", err)
		} else if c.expected_error == nil && id != "foo" {
			t.Errorf("Expected the container ID to be foo, but received %s", id)
		} else if c.expected_error != nil && err == nil {
			t.Errorf("Expected an error, but none received")
		}
	}
}

func TestRelayEnablerStartContainer(t *testing.T) {
	cases := []struct {
		cid            string
		expected_error error
	}{
		{"foo", nil},
		{"bar", nil},
		{"baz", errors.New("1")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing starting container for %s", c.cid), func(t *testing.T) {
			client := MockDockerClient{}
			client.On(
				"ContainerStart",
				context.Background(),
				mock.MatchedBy(
					func(input string) bool {
						return input == c.cid
					},
				),
				types.ContainerStartOptions{},
			).Return(c.expected_error)
			enabler := RelayEnabler{
				Docker: RelayDocker{Client: &client},
			}
			ret := enabler.StartContainer(c.cid)
			client.AssertNumberOfCalls(t, "ContainerStart", 1)
			if c.expected_error == nil && ret != nil {
				t.Errorf("Expected no error, but received %s", ret)
			} else if c.expected_error != nil && ret == nil {
				t.Errorf("Expected an error, but none received")
			}
		})
	}
}
