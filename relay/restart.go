package relay

import (
	"log"
)

func Restart(enabler Enabler, disabler Disabler) error {
	err := Disable(disabler)
	if err != nil {
		log.Printf("Unable to stop the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	err = Enable(enabler)
	if err != nil {
		log.Printf("Unable to start the Relay.")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	log.Printf("Relay restarted!")
	return nil
}
