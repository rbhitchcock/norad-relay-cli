package relay

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
)

type Updater interface {
	CleanUp() error
}

type RelayUpdater struct {
	Image  string
	Docker RelayDocker
}

// CleanUp removes the (now dangling) outdated Relay image
func (updater RelayUpdater) CleanUp() error {
	log.Printf("Removing the old Relay image...")
	var image_without_tag string
	split_image_name := strings.Split(updater.Image, ":")
	if matched, _ := regexp.MatchString(":\\d+/", updater.Image); matched { // a port was included in the name
		image_without_tag = fmt.Sprintf("%s:%s", split_image_name[0], split_image_name[1])
	} else {
		image_without_tag = split_image_name[0]
	}

	args := filters.NewArgs()
	args.Add("reference", image_without_tag)
	args.Add("dangling", "true")
	opts := types.ImageListOptions{
		Filters: args,
	}
	images, err := updater.Docker.Client.ImageList(context.Background(), opts)
	if err != nil {
		log.Printf("Unable to remove: %s", err)
		return err
	}

	for _, i := range images {
		log.Printf("Removing: %s", i.ID)
		_, err := updater.Docker.Client.ImageRemove(context.Background(), i.ID, types.ImageRemoveOptions{})
		if err != nil {
			log.Printf("Unable to remove: %s", err)
			return err
		}
	}
	log.Print("Removed!")
	return nil
}

func Update(updater Updater, installer Installer, enabler Enabler, disabler Disabler) error {
	err := installer.PullImage()
	if err != nil {
		log.Printf("Unable to download the latest Relay image.")
		log.Printf("Relay update failed.")
		return err
	}

	err = Restart(enabler, disabler)
	if err != nil {
		log.Printf("Unable to restart the Relay.")
		log.Printf("Relay update failed.")
		return err
	}

	err = updater.CleanUp()
	if err != nil {
		log.Printf("WARNING: Unable to remove previous Relay Docker image.")
	}

	log.Printf("Relay update successful!")

	return nil
}
