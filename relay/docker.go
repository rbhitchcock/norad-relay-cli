package relay

import (
	"io"
	"log"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
)

type RelayDockerClient interface {
	// Helper Functions for the Client
	ClientVersion() string
	ServerVersion(context.Context) (types.Version, error)

	// Container Functions
	ContainerCreate(ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string) (container.ContainerCreateCreatedBody, error)
	ContainerInspect(context.Context, string) (types.ContainerJSON, error)
	ContainerList(ctx context.Context, options types.ContainerListOptions) ([]types.Container, error)
	ContainerLogs(ctx context.Context, container string, options types.ContainerLogsOptions) (io.ReadCloser, error)
	ContainerRemove(ctx context.Context, container string, options types.ContainerRemoveOptions) error
	ContainerStart(ctx context.Context, container string, options types.ContainerStartOptions) error
	ContainerStop(ctx context.Context, container string, timeout *time.Duration) error

	// Image Functions
	ImageList(context.Context, types.ImageListOptions) ([]types.ImageSummary, error)
	ImagePull(ctx context.Context, ref string, options types.ImagePullOptions) (io.ReadCloser, error)
	ImageRemove(ctx context.Context, image string, options types.ImageRemoveOptions) ([]types.ImageDeleteResponseItem, error)
	ImageInspectWithRaw(context.Context, string) (types.ImageInspect, []byte, error)
}

func NewRelayDockerClient() (*client.Client, error) {
	return client.NewEnvClient()
}

type DockerHelper interface {
	RetrieveTests(bool, bool) ([]types.Container, error)
	RemoveContainers([]types.Container) error
}

type RelayDocker struct {
	Client    RelayDockerClient
	MinVer    string
	ServerVer string
}

func (helper RelayDocker) RetrieveTests(all bool, exited bool) ([]types.Container, error) {
	args := filters.NewArgs()
	args.Add("name", "noradjob*")
	if exited {
		args.Add("status", "exited")
	}
	opts := types.ContainerListOptions{
		All:     all,
		Filters: args,
	}
	return helper.Client.ContainerList(context.Background(), opts)
}

func (helper RelayDocker) RemoveContainers(containers []types.Container) error {
	for i := range containers {
		log.Printf("Removing %s...", containers[i].Names[0])
		err := helper.Client.ContainerRemove(
			context.Background(),
			containers[i].ID,
			types.ContainerRemoveOptions{RemoveVolumes: true, Force: true},
		)
		if err == nil {
			log.Print("OK")
		} else {
			log.Printf("An error occurred while trying to remove the container: %s", err)
			return err
		}
	}
	return nil
}
