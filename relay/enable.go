package relay

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"gopkg.in/yaml.v2"
)

type Enabler interface {
	ValidateInstall() error
	CheckForExisting() error
	LoadContainerConfig() (ComposeService, error)
	CreateContainer(ComposeService) (string, error)
	StartContainer(string) error
}

type RelayEnabler struct {
	Config EnableConfig
	Relay  Relay
	Docker RelayDocker
}

type EnableConfig struct {
	ConfigDir string
	Force     bool
}

func (relay_enabler RelayEnabler) LoadContainerConfig() (ComposeService, error) {
	compose_file := fmt.Sprintf("%s/relay.yml", relay_enabler.Config.ConfigDir)
	log.Printf("Loading the Relay Docker config located at %s.", compose_file)
	contents, err := ioutil.ReadFile(compose_file)
	if err != nil {
		log.Printf("Unable to read Relay file %s.", compose_file)
		return ComposeService{}, err
	}

	config := ComposeConfig{}
	err = yaml.Unmarshal(contents, &config)
	if err != nil {
		log.Printf("Unable to load Relay file %s into config.", compose_file)
		return ComposeService{}, err
	}

	if service, ok := config.Services["relay"]; ok {
		return service, nil
	} else {
		log.Printf("Relay file appears to be outdated.")
		return ComposeService{}, errors.New("Outdated Relay file.")
	}
}

func (relay_enabler RelayEnabler) CreateContainer(service ComposeService) (string, error) {
	volumes := make(map[string]struct{})
	for _, v := range service.Volumes {
		volumes[strings.Split(v, ":")[1]] = struct{}{}
	}
	container_config := container.Config{
		Env:     service.Environment,
		Image:   service.Image,
		Volumes: volumes,
	}
	log_config := container.LogConfig{
		Type:   service.Logging.Driver,
		Config: service.Logging.Options,
	}
	host_config := container.HostConfig{
		RestartPolicy: container.RestartPolicy{Name: service.Restart},
		AutoRemove:    false,
		Binds:         service.Volumes,
		LogConfig:     log_config,
	}
	networking_config := network.NetworkingConfig{}

	create_body, err := relay_enabler.Docker.Client.ContainerCreate(
		context.Background(),
		&container_config,
		&host_config,
		&networking_config,
		service.Container_Name,
	)

	return create_body.ID, err
}

func (relay_enabler RelayEnabler) StartContainer(cid string) error {
	err := relay_enabler.Docker.Client.ContainerStart(
		context.Background(),
		cid,
		types.ContainerStartOptions{},
	)

	return err
}

// ValidateInstall verifies that the Norad Relay has been properly installed.
func (relay_enabler RelayEnabler) ValidateInstall() error {
	config_dir := relay_enabler.Config.ConfigDir

	// make sure the config dir exists
	if _, err := os.Stat(config_dir); os.IsNotExist(err) {
		log.Printf("Specified configuration directory %s does not exist.", config_dir)
		return err
	}

	// make sure compose file exists
	if _, err := os.Stat(fmt.Sprintf("%s/relay.yml", config_dir)); os.IsNotExist(err) {
		log.Printf("No Docker Compose file found in specified configuration directory %s.", config_dir)
		return err
	}

	// make sure configuration file exists
	if _, err := os.Stat(fmt.Sprintf("%s/configuration.yml", config_dir)); os.IsNotExist(err) {
		log.Printf("No configuration file found in specified configuration directory %s.", config_dir)
		return err
	}

	// make sure config file has an org token
	contents, _ := ioutil.ReadFile(fmt.Sprintf("%s/configuration.yml", config_dir))
	config := RelayConfig{}
	err := yaml.Unmarshal(contents, &config)
	if err != nil {
		log.Printf("An error occurred while loading the Relay configuration file in %s.", config_dir)
		return err
	}

	if config.Organization_Token == "" {
		log.Printf("The Relay configuration file in %s is missing its Organization token.", config_dir)
		return errors.New("Organization token is missing from configuration file.")
	}

	return nil
}

// CheckForExisting looks for an existing Norad Relay container. If the force config option is set,
// the existing container will be removed. Otherwise this function will return an error.
func (relay_enabler RelayEnabler) CheckForExisting() error {
	relay_disabler := RelayDisabler{Docker: relay_enabler.Docker}
	container_id, err := relay_disabler.FindContainer()
	if err != nil {
		log.Print("An error occurred while searching for the Norad Relay container.")
		log.Print(err)
		return err
	}
	if container_id == "" {
		log.Print("OK")
		return nil
	}

	if !relay_enabler.Config.Force {
		log.Print("A Norad Relay container already exists.")
		log.Print("Please disable the existing Relay or re-run this command with -f.")
		return errors.New("A Norad Relay container already exists")
	}

	log.Print("Force enable option specified. Removing existing Relay...")
	err = relay_disabler.StopContainer(container_id)
	if err != nil {
		log.Printf("Unable to stop the container %s.", container_id)
		log.Printf("The following error occurred: %s", err)
		return err
	}
	err = relay_disabler.RemoveContainer(container_id)
	if err != nil {
		log.Printf("Unable to remove the container %s.", container_id)
		log.Printf("The following error occurred: %s", err)
		return err
	}

	return nil
}

func Enable(enabler Enabler) error {
	err := enabler.ValidateInstall()
	if err != nil {
		return err
	}

	err = enabler.CheckForExisting()
	if err != nil {
		return err
	}

	service, err := enabler.LoadContainerConfig()
	if err != nil {
		log.Print("An error occurred while loading the Relay container config file.")
		log.Print(err)
		return err
	}

	container_id, err := enabler.CreateContainer(service)
	if err != nil || container_id == "" {
		log.Print("Unable to create the Norad Relay container.")
		log.Printf("The following error occurred: %s.", err)
		return err
	}

	err = enabler.StartContainer(container_id)
	if err == nil {
		log.Print("Norad Relay successfully enabled!")
	} else {
		log.Print("Unable to start the Norad Relay container.")
		log.Printf("The following error occurred: %s.", err)
		return err
	}

	return nil
}
