package relay

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"gopkg.in/yaml.v2"
)

type Debugger interface {
	LoadConfig() (ComposeConfig, error)
	WriteConfig(ComposeConfig) error
	CleanupExited() error
}

type RelayDebugger struct {
	Enable    bool
	Cleanup   bool
	ConfigDir string
	Docker    DockerHelper
}

func (relay_debugger RelayDebugger) LoadConfig() (ComposeConfig, error) {
	fn := fmt.Sprintf("%s/relay.yml", relay_debugger.ConfigDir)
	config := ComposeConfig{}
	contents, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Printf("Unable to load Relay configuration.")
		return config, err
	}
	err = yaml.Unmarshal(contents, &config)
	if err != nil {
		log.Printf("Unable to load Relay configuration.")
		return config, err
	}
	return config, err
}

func (relay_debugger RelayDebugger) WriteConfig(config ComposeConfig) error {
	fn := fmt.Sprintf("%s/relay.yml", relay_debugger.ConfigDir)
	mode_found := false
	var new_env []string

	for _, e := range config.Services["relay"].Environment {
		if e == "RELAY_MODE=production" && relay_debugger.Enable {
			mode_found = true
			log.Printf("Production mode setting found. Changing to debug mode.")
			new_env = append(new_env, "RELAY_MODE=debug")
		} else if e == "RELAY_MODE=debug" && !relay_debugger.Enable {
			mode_found = true
			log.Printf("Debug mode setting found. Changing to production mode.")
			new_env = append(new_env, "RELAY_MODE=production")
		} else if strings.Contains(e, "RELAY_MODE") {
			mode_found = true
			new_env = append(new_env, e)
		} else {
			new_env = append(new_env, e)
		}
	}

	// We need to handle the case that the relay mode is somehow missing completely
	if !mode_found && relay_debugger.Enable {
		new_env = append(new_env, "RELAY_MODE=debug")
	} else if !mode_found {
		new_env = append(new_env, "RELAY_MODE=production")
	}

	service, ok := config.Services["relay"]
	if !ok {
		log.Printf("Unable to load configuration file.")
		return errors.New("No Relay service exists in configuration file.")
	}
	service.Environment = new_env
	config.Services["relay"] = service
	d, err := yaml.Marshal(&config)
	if err != nil {
		log.Printf("Unable to load Relay configuration.")
		return err
	}
	err = ioutil.WriteFile(fn, d, 0600)
	if err != nil {
		log.Printf("Unable to write the Relay configuration.")
		return err
	}
	return err
}

func (relay_debugger RelayDebugger) CleanupExited() error {
	if relay_debugger.Enable || !relay_debugger.Cleanup {
		return nil
	}
	log.Printf("Cleaning up exited tests left behind by debug mode...")
	containers, err := relay_debugger.Docker.RetrieveTests(false, true)
	if err != nil {
		log.Printf("An error occurred while trying to build the list of exited tests.")
		log.Printf("The following error occurred: %s", err)
		return err
	}
	if len(containers) == 0 {
		log.Printf("No tests found.")
		return nil
	}
	log.Printf("Removing %d tests...", len(containers))
	return relay_debugger.Docker.RemoveContainers(containers)
}

func ToggleDebug(debugger Debugger, enabler Enabler, disabler Disabler) error {
	config, err := debugger.LoadConfig()
	if err != nil {
		log.Printf("The following error ocurred: %s", err)
		return err
	}
	err = debugger.WriteConfig(config)
	if err != nil {
		log.Printf("The following error ocurred: %s", err)
		return err
	}
	err = Restart(enabler, disabler)
	if err != nil {
		log.Printf("The following error ocurred: %s", err)
		return err
	}
	log.Printf("Relay mode successfully changed!")

	err = debugger.CleanupExited()
	return err
}
