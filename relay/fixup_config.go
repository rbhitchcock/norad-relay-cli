package relay

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

type MalformedRelayService struct {
	Relay ComposeService
}

type Fixer interface {
	LoadMalformedConfig() (ComposeService, error)
	WriteFixedConfig(ComposeService) error
}

type RelayConfigFixer struct {
	ConfigDir string
}

func (fixer RelayConfigFixer) LoadMalformedConfig() (ComposeService, error) {
	compose_file := fmt.Sprintf("%s/relay.yml", fixer.ConfigDir)
	log.Printf("Attempting to load an outdated %s Relay Docker file.", compose_file)
	contents, err := ioutil.ReadFile(compose_file)
	if err != nil {
		log.Printf("Unable to read Relay file %s.", compose_file)
		return ComposeService{}, err
	}

	malformed_relay_service := MalformedRelayService{}
	err = yaml.Unmarshal(contents, &malformed_relay_service)
	if err != nil {
		log.Printf("Unable to load Relay file %s into config.", compose_file)
		return ComposeService{}, err
	}
	if malformed_relay_service.Relay.Container_Name == "" {
		log.Printf("Unable to load Relay file %s into config.", compose_file)
		return ComposeService{}, errors.New("File is missing required fields.")
	}
	return malformed_relay_service.Relay, nil
}

func (fixer RelayConfigFixer) WriteFixedConfig(service ComposeService) error {
	service_map := make(map[string]ComposeService)
	fn := fmt.Sprintf("%s/relay.yml", fixer.ConfigDir)
	log.Printf("Generating new Relay Docker configuration file and writing to %s...", fn)

	// We didn't always explicitly set the environment or the log config
	if service.Environment == nil {
		service.Environment = []string{"RELAY_MODE=production"}
	}
	if service.Logging.Driver == "" {
		service.Logging = RelayLogConfig{
			Driver: "json-file",
			Options: map[string]string{
				"max-size": "20m",
				"max-file": "5",
			},
		}
	}
	service.Restart = "always" // just in case it's a different setting
	service_map["relay"] = service
	config := ComposeConfig{
		Version:  "3",
		Services: service_map,
	}

	d, err := yaml.Marshal(&config)
	if err != nil {
		log.Print("Unable to generate YAML for Compose file.")
		return err
	}

	err = ioutil.WriteFile(fn, d, 0600)
	if err != nil {
		log.Printf("Unable to write to Relay Docker configuration file %s.", fn)
		return err
	}

	log.Printf("Relay Docker configuration file successfully fixed and written to %s.", fn)
	return err
}

func FixupConfig(fixer Fixer, enabler Enabler) error {
	_, err := enabler.LoadContainerConfig()
	if err == nil {
		log.Printf("Format of Relay configuration appears correct. Nothing to do.")
		return nil
	} else if err.Error() != "Outdated Relay file." {
		log.Printf("An unexpected error occurred: %s", err)
		log.Printf("Ensure the file exists.")
		return err
	}
	service, err := fixer.LoadMalformedConfig()
	if err != nil {
		log.Printf("An error occurred while trying to load the config file: %s", err)
		return err
	}

	err = fixer.WriteFixedConfig(service)
	if err != nil {
		log.Printf("An error occurred while trying to write the config file: %s", err)
		return err
	}

	return err
}
