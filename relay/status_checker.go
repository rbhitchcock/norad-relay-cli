package relay

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"gitlab.com/norad/relay-cli/registry"
)

// StatusChecker is the interface for checking the status of an entity.
type StatusChecker interface {
	InstallStatus() error
	EnableStatus() error
	RunningStatus() error
	CheckVersion() error
}

// RelayStatusChecker implements the functions needed to determine the current status of the Relay.
type RelayStatusChecker struct {
	ConfigDir   string
	Docker      RelayDocker
	Registry    RelayRegistry
	RegistryURI string
	RelayImage  string
}

// InstallStatus returns whether or not the Relay installation can be detected.
func (checker RelayStatusChecker) InstallStatus() error {
	log.Printf("Attempting to detect installation in directory %s...", checker.ConfigDir)
	_, err := os.Stat(checker.ConfigDir)

	if err != nil {
		log.Printf("The installation directory %s does not exist or is not readable.", checker.ConfigDir)
		return err
	}

	relay_file := fmt.Sprintf("%s/relay.yml", checker.ConfigDir)
	_, err = os.Stat(relay_file)
	if err != nil {
		log.Printf("The Relay configuration file relay.yml in %s does not exist or is not readable.", checker.ConfigDir)
		return err

	}

	log.Print("The Relay appears to be installed!")
	return nil
}

// EnableStatus detects if a container by the name of norad_relay exists.
func (checker RelayStatusChecker) EnableStatus() error {
	log.Printf("Checking if the Relay is enabled...")
	container_id, err := findRelayContainer(checker.Docker.Client, false)
	if err != nil {
		log.Printf("An error occurred while checking the enabled status of the Relay: %s.", err)
		return err
	}

	if container_id == "" {
		log.Printf("Could not find an enabled Relay.")
		return errors.New("could not find an enabled Relay")
	}

	log.Print("The Relay is enabled!")
	return err
}

// EnableStatus detects if a runing container by the name of norad_relay exists.
func (checker RelayStatusChecker) RunningStatus() error {
	log.Printf("Checking if the Relay is running...")
	container_id, err := findRelayContainer(checker.Docker.Client, true)
	if err != nil {
		log.Printf("An error occurred while checking the running status of the Relay: %s.", err)
		return err
	}

	if container_id == "" {
		log.Printf("Could not find a running Relay.")
		return errors.New("could not find a running Relay")
	}

	mode := "PRODUCTION"
	container, err := checker.Docker.Client.ContainerInspect(context.Background(), container_id)
	if err != nil {
		log.Printf("Unable to determine the current mode of the Relay due to an error: %s.", err)
		mode = ""
	}
	if container.Config != nil {
		for _, e := range container.Config.Env {
			if e == "RELAY_MODE=production" {
				mode = "PRODUCTION"
				break
			} else if e == "RELAY_MODE=debug" {
				mode = "DEBUG"
				break
			}
		}
	} else {
		log.Printf("Unable to determine the current mode of the Relay.")
		mode = ""
	}

	if mode != "" {
		log.Printf("The Relay is running in %s mode!", mode)
	} else {
		log.Print("The Relay is running!")
	}

	return err
}

// CheckVersion compares the local Relay image what what is available in the Registry
func (checker RelayStatusChecker) CheckVersion() error {
	log.Printf("Checking if an update is available for the Relay...")
	log.Printf("Connecting to the registry at %s to look up the latest version...", checker.RegistryURI)

	if checker.RegistryURI == "" {
		checker.RegistryURI = "https://norad-registry.cisco.com:5000"
	}

	if checker.RelayImage == "" {
		checker.RelayImage = "relay:latest"
	}

	if checker.Registry == nil {
		hub, err := registry.New(checker.RegistryURI, "", "")
		if err != nil {
			log.Printf("Error occurred while connecting to registry: %s", err)
			return err
		}
		checker.Registry = hub
	}
	fully_qualified_image_name := checker.imageNameWithoutProtocol()
	image_name, image_tag := checker.splitImageName()

	manifest, err := checker.Registry.ManifestV2(image_name, image_tag)
	if err != nil {
		log.Printf("An error occurred while looking up the latest version: %s", err)
		return err
	}

	log.Printf("Comparing latest version to local version...")
	image_details, _, err := checker.Docker.Client.ImageInspectWithRaw(context.Background(), fully_qualified_image_name)
	if err != nil {
		log.Printf("Unable to look up the Relay Docker image on your host. Is it downloaded?")
		log.Printf("The following error occurred: %s", err)
		return err
	}

	if image_details.ID == manifest.Config.Digest.String() {
		log.Printf("You are running the latest version of the Relay!")
	} else {
		log.Printf("A new version of the Relay is available!")
	}
	return nil
}

func (checker RelayStatusChecker) imageNameWithoutProtocol() string {
	re := regexp.MustCompile("^https?://")
	subbed := re.ReplaceAllString(checker.RegistryURI, "")
	return fmt.Sprintf("%s/%s", subbed, checker.RelayImage)
}

func (checker RelayStatusChecker) splitImageName() (string, string) {
	split_image_name := strings.Split(checker.RelayImage, ":")
	image_name := split_image_name[0]
	image_tag := ""
	if len(split_image_name) > 1 {
		image_tag = split_image_name[1]
	}
	return image_name, image_tag
}

// CheckStatus prints the current status of the entity the checker is testing.
func CheckStatus(checker StatusChecker) error {
	err := checker.InstallStatus()
	if err != nil {
		return err
	}
	fmt.Println()

	err = checker.CheckVersion()
	if err != nil {
		return err
	}
	fmt.Println()

	err = checker.EnableStatus()
	if err != nil {
		return err
	}
	fmt.Println()

	err = checker.RunningStatus()
	if err != nil {
		return err
	}
	fmt.Println()

	return nil
}
