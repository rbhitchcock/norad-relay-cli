package relay

type ComposeConfig struct {
	Version  string
	Services map[string]ComposeService
}

type ComposeService struct {
	Restart        string
	Container_Name string
	Image          string
	Volumes        []string
	Environment    []string
	Logging        RelayLogConfig
}

type RelayLogConfig struct {
	Driver  string
	Options map[string]string
}
