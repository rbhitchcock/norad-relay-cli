package relay

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"testing"

	"github.com/docker/distribution"
	manifest "github.com/docker/distribution/manifest/schema2"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/mock"
)

type MockChecker struct {
	mock.Mock
}

func (checker MockChecker) InstallStatus() error {
	args := checker.Called()
	return args.Error(0)
}

func (checker MockChecker) EnableStatus() error {
	args := checker.Called()
	return args.Error(0)
}

func (checker MockChecker) RunningStatus() error {
	args := checker.Called()
	return args.Error(0)
}

func (checker MockChecker) CheckVersion() error {
	args := checker.Called()
	return args.Error(0)
}

func TestCheckStatus(t *testing.T) {
	cases := []struct {
		install_status error
		enable_status  error
		running_status error
		check_version  error
		expectation    error
	}{
		{install_status: errors.New("install status"), expectation: errors.New("install status")},
		{enable_status: errors.New("enable status"), expectation: errors.New("enable status")},
		{running_status: errors.New("running status"), expectation: errors.New("running status")},
		{check_version: errors.New("check version"), expectation: errors.New("check version")},
		{nil, nil, nil, nil, nil},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing execution halting for method %s", c.expectation), func(t *testing.T) {
			testObj := new(MockChecker)
			testObj.On("InstallStatus").Return(c.install_status)
			testObj.On("EnableStatus").Return(c.enable_status)
			testObj.On("RunningStatus").Return(c.running_status)
			testObj.On("CheckVersion").Return(c.check_version)
			ret := CheckStatus(testObj)
			if c.expectation != nil {
				if ret == nil || ret.Error() != c.expectation.Error() {
					t.Errorf("Expected Error %s, got %s", c.expectation, ret)
				}
			} else if ret != nil {
				t.Errorf("Expected nil, got %s", ret)
			}
		})
	}
}

func TestRelayStatusCheckerInstallStatus(t *testing.T) {
	valid_test_dir := "/etc/relay_test_dir"
	dne_test_dir := "/etc/relay_test_dir_dne"

	cases := []struct {
		test_dir          string
		create_relay_file bool
		file_mask         os.FileMode
		expectation       error
	}{
		{test_dir: valid_test_dir, create_relay_file: true, expectation: nil},
		{test_dir: dne_test_dir, expectation: errors.New("no such file or directory")},
		{test_dir: valid_test_dir, expectation: errors.New("relay.yml: no such file or directory")},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Testing InstallStatus for %s with expected error %s.", c.test_dir, c.expectation), func(t *testing.T) {
			if c.test_dir == valid_test_dir {
				os.Mkdir(c.test_dir, 0700)
				defer os.RemoveAll(c.test_dir)
			}
			if c.create_relay_file {
				rf, _ := os.Create(fmt.Sprintf("%s/relay.yml", c.test_dir))
				rf.Chmod(c.file_mask)
			}

			checker := RelayStatusChecker{ConfigDir: c.test_dir}
			err := checker.InstallStatus()

			if c.expectation != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectation.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectation, err)
				}
			} else if err != nil {
				t.Errorf("Expected nil, got %s", err)
			}
		})
	}
}

func TestRelayStatusCheckerEnableStatus(t *testing.T) {
	cases := []struct {
		hasRelay      bool
		numContainers int
		expectedError error
		listError     error
	}{
		{numContainers: 2, expectedError: errors.New("could not find an enabled Relay")},
		{numContainers: 2, hasRelay: true, listError: errors.New("list_error"), expectedError: errors.New("list_error")},
		{expectedError: errors.New("could not find an enabled Relay")},
		{numContainers: 2, hasRelay: true},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Searching for Relay container expecting error %s", c.expectedError), func(t *testing.T) {
			containers := containersGeneratorForFind(c.hasRelay, c.numContainers)
			client := MockDockerClient{}
			client.On(
				"ContainerList",
				context.Background(),
				types.ContainerListOptions{All: true},
			).Return(containers, c.listError)
			checker := RelayStatusChecker{
				Docker: RelayDocker{Client: &client},
			}
			err := checker.EnableStatus()
			if c.expectedError == nil {
				if err != nil {
					t.Errorf("Expected no error, got %s", err)
				}
			} else {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			}
		})
	}
}

func TestRelayStatusCheckerRunningStatus(t *testing.T) {
	cases := []struct {
		hasRelay        bool
		numContainers   int
		expectedMode    string
		containerConfig *container.Config
		expectedError   error
		listError       error
		inspectError    error
	}{
		{numContainers: 2, expectedError: errors.New("could not find a running Relay")},
		{numContainers: 2, hasRelay: true, listError: errors.New("list_error"), expectedError: errors.New("list_error")},
		{expectedError: errors.New("could not find a running Relay")},
		{
			numContainers: 2,
			hasRelay:      true,
			inspectError:  errors.New("inspect_error"),
			expectedError: errors.New("inspect_error"),
		},
		{
			numContainers:   1,
			hasRelay:        true,
			containerConfig: &container.Config{Env: []string{"foo"}},
			expectedMode:    "production",
		},
		{
			numContainers:   1,
			hasRelay:        true,
			containerConfig: &container.Config{Env: []string{}},
			expectedMode:    "production",
		},
		{
			numContainers:   1,
			hasRelay:        true,
			containerConfig: &container.Config{Env: []string{"RELAY_MODE=production"}},
			expectedMode:    "production",
		},
		{
			numContainers:   1,
			hasRelay:        true,
			containerConfig: &container.Config{Env: []string{"RELAY_MODE=debug"}},
			expectedMode:    "debug",
		},
		{numContainers: 2, hasRelay: true},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("Searching for Relay container expecting error %s", c.expectedError), func(t *testing.T) {
			containers := containersGeneratorForFind(c.hasRelay, c.numContainers)
			client := MockDockerClient{}
			client.On(
				"ContainerList",
				context.Background(),
				types.ContainerListOptions{All: false},
			).Return(containers, c.listError)
			client.On(
				"ContainerInspect",
				context.Background(),
				"therelay",
			).Return(types.ContainerJSON{Config: c.containerConfig}, c.inspectError)
			checker := RelayStatusChecker{
				Docker: RelayDocker{Client: &client},
			}
			var str bytes.Buffer
			log.SetOutput(&str)

			err := checker.RunningStatus()

			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected no error, got %s", err)
			} else if c.expectedMode == "" && !strings.Contains(str.String(), "The Relay is running!") {
				t.Errorf("Expected to not be able to detect the Relay mode.")
			} else if c.expectedMode == "production" && !strings.Contains(str.String(), "PRODUCTION") {
				t.Errorf("Expected to detect production mode.")
			} else if c.expectedMode == "debug" && !strings.Contains(str.String(), "DEBUG") {
				t.Errorf("Expected to detect debug mode.")
			}
		})
	}
}

func TestRelayStatusCheckerCheckVersion(t *testing.T) {
	cases := []struct {
		registryURI        string
		useRegistryPackage bool
		imageName          string
		localVersion       string
		remoteVersion      digest.Digest
		remoteLookupError  error
		localLookupError   error
		expectedError      error
	}{
		{registryURI: ":invalid.invalid", useRegistryPackage: true, expectedError: errors.New("missing protocol scheme")},
		{remoteLookupError: errors.New("remote lookup"), expectedError: errors.New("remote lookup")},
		{localLookupError: errors.New("local lookup"), expectedError: errors.New("local lookup")},
		{remoteVersion: "2", localVersion: "a"},
		{remoteVersion: "d", localVersion: "d"},
		{registryURI: "http://invalid.invalid", remoteVersion: "d", localVersion: "d"},
		{registryURI: "invalid.invalid", remoteVersion: "d", localVersion: "d"},
		{imageName: "foo:bar", remoteVersion: "d", localVersion: "d"},
		{imageName: "notag", remoteVersion: "d", localVersion: "d"},
		{imageName: "notag", remoteVersion: "d", localVersion: "0"},
	}

	for _, c := range cases {
		t.Run("Testing version checker", func(t *testing.T) {
			uri := "https://norad-registry.cisco.com:5000"
			if c.registryURI != "" {
				uri = c.registryURI
			}
			re := regexp.MustCompile("^https?://")
			uri_without_protocol := re.ReplaceAllString(uri, "")
			image := "relay:latest"
			if c.imageName != "" {
				image = c.imageName
			}
			expected_image_and_tag := strings.Split(image, ":")
			if len(expected_image_and_tag) < 2 {
				expected_image_and_tag = append(expected_image_and_tag, "")
			}
			client := MockDockerClient{}
			local_image_details := types.ImageInspect{ID: c.localVersion}
			client.On(
				"ImageInspectWithRaw",
				context.Background(),
				fmt.Sprintf("%s/%s", uri_without_protocol, image),
			).Return(local_image_details, []byte{}, c.localLookupError)
			checker := RelayStatusChecker{
				RegistryURI: c.registryURI,
				RelayImage:  c.imageName,
				Docker:      RelayDocker{Client: &client},
			}
			if !c.useRegistryPackage {
				deserialized := manifest.DeserializedManifest{
					Manifest: manifest.Manifest{
						Config: distribution.Descriptor{Digest: c.remoteVersion},
					},
				}
				registry := &MockRegistry{}
				registry.On(
					"ManifestV2",
					expected_image_and_tag[0],
					expected_image_and_tag[1],
				).Return(&deserialized, c.remoteLookupError)
				checker.Registry = registry
			}

			var str bytes.Buffer
			log.SetOutput(&str)
			err := checker.CheckVersion()

			if c.expectedError != nil {
				if err == nil || !strings.Contains(err.Error(), c.expectedError.Error()) {
					t.Errorf("Expected error %s, but got %s", c.expectedError, err)
				}
			} else if err != nil {
				t.Errorf("Expected nil, got %s", err)
			} else if c.remoteVersion.String() == c.localVersion && !strings.Contains(str.String(), "You are running the latest version of the Relay!") {
				t.Errorf("Should report Relay is up to date")
			} else if c.remoteVersion.String() != c.localVersion && strings.Contains(str.String(), "You are running the latest version of the Relay!") {
				t.Errorf("Should report Relay is outdated when using %s and %s", c.remoteVersion, c.localVersion)
			}
		})
	}
}
