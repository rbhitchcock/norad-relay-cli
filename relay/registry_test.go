package relay

import (
	manifest "github.com/docker/distribution/manifest/schema2"
	"github.com/stretchr/testify/mock"
)

type MockRegistry struct {
	mock.Mock
}

func (r *MockRegistry) ManifestV2(image string, tag string) (*manifest.DeserializedManifest, error) {
	ret := r.Called(image, tag)
	return ret.Get(0).(*manifest.DeserializedManifest), ret.Error(1)
}
