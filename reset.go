package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func resetHandler(c *cli.Context) error {
	org_token := c.String("organization-token")
	if org_token == "" {
		org_token = getOrgToken()
	}

	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	installer := relay.RelayInstaller{
		Docker: relay.RelayDocker{
			Client:    docker_client,
			MinVer:    "1.13.0-0",
			ServerVer: getDockerServerVer(docker_client),
		},
		Config: relay.InstallConfig{
			InstallDir: c.String("dir"),
			DryRun:     false,
			Enable:     true,
		},
		Relay: relay.Relay{
			Image: c.String("relay-image"),
			Config: relay.RelayConfig{
				API_URI:                      c.String("api-uri"),
				Organization_Token:           org_token,
				Private_Key:                  "/relay/relay.pem",
				DB:                           "/relay/relay.store",
				Queue_User:                   c.String("queue-user"),
				Queue_Password:               c.String("queue-password"),
				Whitelisted_Repository_Hosts: []string{"norad-registry.cisco.com"},
			},
		},
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}
	uninstaller := relay.RelayUninstaller{
		Disabler: disabler,
		Relay: relay.Relay{
			Image: c.String("relay-image"),
		},
		Config: relay.UninstallConfig{
			Dir: c.String("dir"),
		},
	}

	if c.Bool("force") || confirmReset(c.String("dir")) {
		return relay.Reset(installer, uninstaller, disabler)
	} else {
		log.Printf("Exiting without reseting.")
		return nil
	}
}

func confirmReset(dir string) bool {
	fmt.Printf("Are you sure you want to reset the Relay and delete the directory %s? Please enter 'yes' or 'no'.\n", dir)
	return confirmAction()
}
