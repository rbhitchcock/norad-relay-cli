package registry

import (
	"io/ioutil"
	"net/http"

	manifest "github.com/docker/distribution/manifest/schema2"
)

func (registry *Registry) ManifestV2(repository, reference string) (*manifest.DeserializedManifest, error) {
	url := registry.url("/v2/%s/manifests/%s", repository, reference)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Accept", manifest.MediaTypeManifest)
	resp, err := registry.Client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	deserialized := &manifest.DeserializedManifest{}
	err = deserialized.UnmarshalJSON(body)
	if err != nil {
		return nil, err
	}
	return deserialized, nil
}
