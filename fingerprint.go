package main

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func fingerprintHandler(c *cli.Context) error {
	fn := fmt.Sprintf("%s/relay.pem", c.String("dir"))
	key := relay.RelaySigningKey{File: fn}

	err := key.InitializeKey()
	if err != nil {
		log.Printf("Unable to initialize the key. Please ensure the key file exists.")
		log.Print(err)
		return err
	}

	fp, err := key.Fingerprint()
	if err != nil {
		log.Printf("An unexpected error occurred. The fingerprint could not be generated.")
		log.Printf("The following error ooccurred: %s.", err)
		return err
	}

	if len(fp) != sha256.Size {
		log.Printf("An unexpected error occurred. The fingerprint could not be generated.")
		return errors.New("Invalid fingerprint returned.")
	}

	fmt.Println("Full fingerprint:")
	fmt.Printf("  ")
	for i, b := range fp {
		fmt.Printf("%02X", b)
		if i < len(fp)-1 {
			fmt.Print(":")
		}
	}
	fmt.Println()
	fmt.Println("Truncated fingerprint:")
	fmt.Printf("  ")
	for i, b := range fp[24:] {
		fmt.Printf("%02X", b)
		if i < len(fp[24:])-1 {
			fmt.Print(":")
		}
	}
	fmt.Println()
	return nil
}
