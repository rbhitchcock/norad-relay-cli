package main

import (
	"log"
	"time"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func systemCheckHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	timeout := time.Duration(30) * time.Second
	err = relay.SystemCheck(
		relay.RelaySystemChecker{
			ApiUri:       c.String("api-uri"),
			RegistryUris: c.StringSlice("registry-uris"),
			ConnTimeout:  timeout,
			Docker: relay.RelayDocker{
				Client: docker_client,
			},
		},
	)
	if err != nil {
		log.Printf("WARNING: One or more system compatibility checks failed.")
	} else {
		log.Printf("Congratulations! This system is properly configured to support the Norad Relay.")
	}
	return err
}
