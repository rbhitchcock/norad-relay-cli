package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func uninstallHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}
	uninstaller := relay.RelayUninstaller{
		Disabler: disabler,
		Relay: relay.Relay{
			Image: c.String("relay-image"),
		},
		Config: relay.UninstallConfig{
			Dir: c.String("dir"),
		},
	}
	if c.Bool("force") || confirmUninstall(c.String("dir")) {
		return relay.Uninstall(uninstaller)
	} else {
		log.Printf("Exiting without disabling.")
		return nil
	}
}

func confirmUninstall(dir string) bool {
	fmt.Printf("Are you sure you want to disable the Relay and delete the directory %s? Please enter 'yes' or 'no'.\n", dir)
	return confirmAction()
}
