package main

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/norad/relay-cli/relay"
)

func confirmAction() bool {
	var token string
	_, err := fmt.Scanln(&token)
	if err != nil || strings.ToLower(token) != "yes" {
		return false
	}
	return true
}

func getDockerServerVer(docker_client relay.RelayDockerClient) string {
	docker_version, err := docker_client.ServerVersion(context.Background())
	if err != nil {
		panic(err)
	}
	return docker_version.Version
}

func getOrgToken() string {
	fmt.Println("An Organization token is required to register a Relay. Please provide it now:")
	var token string
	_, err := fmt.Scanln(&token)
	if err != nil || token == "" {
		return getOrgToken()
	}
	return token
}
