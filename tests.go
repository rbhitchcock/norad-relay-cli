package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func testsHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}
	relay_docker := relay.RelayDocker{Client: docker_client}

	msg := "Looking for %s tests..."
	if c.Bool("all") {
		log.Printf(msg, "all")
	} else {
		log.Printf(msg, "in progress")
	}

	containers, err := relay_docker.RetrieveTests(c.Bool("all"), false)
	if err != nil {
		log.Printf("Unable to get a list of tests.")
		log.Printf("The following error occurred: %s", err)
		return err
	}
	if len(containers) == 0 {
		log.Printf("No tests found.")
		return nil
	}
	for i := range containers {
		fmt.Printf("\nTest %d\n", i+1)
		fmt.Printf(
			"\tID: %-50s\n\tStatus: %-25s\n\tTest: %-50s\n",
			strings.Trim(containers[i].Names[0], "/"),
			containers[i].Status,
			containers[i].Image,
		)
	}

	fmt.Println()
	log.Printf("Test search complete.")
	return nil
}
