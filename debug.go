package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func debugHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	debugger := relay.RelayDebugger{
		Enable:    !c.Bool("disable"),
		Cleanup:   !c.Bool("no-cleanup"),
		ConfigDir: c.String("dir"),
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	enabler := relay.RelayEnabler{
		Config: relay.EnableConfig{
			ConfigDir: c.String("dir"),
			Force:     false,
		},
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	disabler := relay.RelayDisabler{
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
	}

	if c.Bool("force") || confirmDebug(c.Bool("disable"), !c.Bool("no-cleanup")) {
		return relay.ToggleDebug(debugger, enabler, disabler)
	} else {
		log.Printf("Exiting without changing mode.")
		return nil
	}
}

func confirmDebug(disable bool, cleanup bool) bool {
	msg := "enable"
	cleanup_msg := ""
	if disable {
		msg = "disable"
		if cleanup {
			cleanup_msg = "Completed test containers will also be removed. "
		}
	}
	fmt.Printf("Are you sure you want to %s the debug mode of the Relay? %sPlease enter 'yes' or 'no'.\n", msg, cleanup_msg)
	return confirmAction()
}
