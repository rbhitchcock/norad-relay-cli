package main

import (
	"fmt"
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func fixupConfigHandler(c *cli.Context) error {
	enabler := relay.RelayEnabler{
		Config: relay.EnableConfig{
			ConfigDir: c.String("dir"),
		},
	}
	fixer := relay.RelayConfigFixer{ConfigDir: c.String("dir")}

	if c.Bool("force") || confirmFixup(c.String("dir")) {
		return relay.FixupConfig(fixer, enabler)
	} else {
		log.Printf("Exiting without fixing.")
		return nil
	}
}

func confirmFixup(dir string) bool {
	fmt.Printf("Are you sure you want to regenerate the Relay Docker configuration file %s/relay.yml? Please enter 'yes' or 'no'.\n", dir)
	return confirmAction()
}
