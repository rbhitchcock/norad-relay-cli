package main

import (
	"log"

	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

func statusHandler(c *cli.Context) error {
	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}
	checker := relay.RelayStatusChecker{
		ConfigDir: c.String("dir"),
		Docker: relay.RelayDocker{
			Client: docker_client,
		},
		RegistryURI: c.String("registry-uri"),
		RelayImage:  c.String("relay-image"),
	}
	err = relay.CheckStatus(checker)

	if err != nil {
		log.Printf("Unable to initialize the key. Please ensure the key file exists.")
		log.Print(err)
		return err
	}
	return nil
}
