[![pipeline
status](https://gitlab.com/norad/relay-cli/badges/master/pipeline.svg)](https://gitlab.com/norad/relay-cli/commits/master) [![coverage
report](https://gitlab.com/norad/relay-cli/badges/master/coverage.svg)](https://gitlab.com/norad/relay-cli/commits/master)

# relay-cli

A CLI tool for managing your Norad Relay installation. This utility allows you to perform system diagnostic checks, install Relays, reset configuration, and more.

## Relay Documentation

Documentation about the Relay, system requirements, etc, can be found
[here](https://norad.gitlab.io/docs/#norad-relay).

## Relay CLI Download

To download the latest version of the Relay CLI, use the link that matches your platform.

### Linux
* [32 bit](https://gitlab.com/norad/relay-cli/-/jobs/artifacts/master/raw/build/relay?job=build_linux_386)
* [64 bit](https://gitlab.com/norad/relay-cli/-/jobs/artifacts/master/raw/build/relay?job=build_linux_amd64)

## Installation of the CLI tool

Download (or build) the executable appropriate for your system and put it somewhere in your `PATH`.

The following example assumes a 64-bit Linux operating system.

```sh
# Update system and ensure curl is installed
$> sudo apt-get update && sudo apt-get install -y curl
$> curl -L -o relay https://gitlab.com/norad/relay-cli/-/jobs/artifacts/master/raw/build/relay?job=build_linux_amd64
$> sudo chown root:root relay
$> sudo chmod 770 relay
$> sudo mv relay /usr/local/bin/relay
$> sudo relay --help
NAME:
   relay - manage your Norad Relay installation

USAGE:
   relay [global options] command [command options] [arguments...]

VERSION:
   0.0.1

COMMANDS:
     debug         enables and disables debug mode for the Relay
     disable       disables the Norad Relay
     enable        enables the Norad Relay
     fingerprint   prints the Relay key fingerprint
     fixup-config  attempts to generate a valid Relay Docker configuration file
     install       installs the Norad Relay to the host
     logs          prints the Relay logs
     reset         reinstalls the Norad Relay
     restart       restarts the Norad Relay
     status        prints the Relay status
     system-check  perform a system diagnostics check for compatibility with the Norad Relay
     tests         prints the container IDs for tests running on the Relay
     uninstall     removes the Norad Relay from the host
     update        updates the Norad Relay to the latest version
     help, h       Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

Congratulations! You've successfully installed the Relay CLI utility.

## Usage

### Getting Help

Any time you are unsure what a command does, you can use the `--help` flag to see more information for that command. Running `relay --help` prints the list of actions that are available.

```sh
$> sudo relay --help
NAME:
   relay - manage your Norad Relay installation

USAGE:
   relay [global options] command [command options] [arguments...]

VERSION:
   0.0.1

COMMANDS:
     debug         enables and disables debug mode for the Relay
     disable       disables the Norad Relay
     enable        enables the Norad Relay
     fingerprint   prints the Relay key fingerprint
     fixup-config  attempts to generate a valid Relay Docker configuration file
     install       installs the Norad Relay to the host
     logs          prints the Relay logs
     reset         reinstalls the Norad Relay
     restart       restarts the Norad Relay
     system-check  perform a system diagnostics check for compatibility with the Norad Relay
     tests         prints the container IDs for tests running on the Relay
     uninstall     removes the Norad Relay from the host
     update        updates the Norad Relay to the latest version
     help, h       Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

### Peform a System Check

The Norad Relay has a few connectivity requirements. In order to verify that your system and network is properly configured, use the `system-check` command.

```sh
$> sudo relay system-check
2017/10/03 21:44:00 Successfully connected to the Docker daemon.
2017/10/03 21:44:00 Attempting to connect to the API host at norad.cisco.com:8443.
2017/10/03 21:44:00 Successfully connected to the API host at norad.cisco.com:8443.
2017/10/03 21:44:00 Attempting to connect to the Registry host at norad-registry.cisco.com:5000.
2017/10/03 21:44:00 Successfully connected to the Registry host at norad-registry.cisco.com:5000.
2017/10/03 21:44:00 Attempting to connect to the Registry host at norad-registry.cisco.com:5001.
2017/10/03 21:44:00 Successfully connected to the Registry host at norad-registry.cisco.com:5001.
2017/10/03 21:44:00 Congratulations! This system is properly configured to support the Norad Relay.
```

### Fix Up an Existing Config

If you've installed the Relay via a separate process (in other words, you installed it before using
this CLI tool), you may have an outdated config file. The `fixup-config` command will attempt to
correct the format. Please note that this may result in data loss, and in some cases you may have
to reset your Relay installation.

```sh
$> sudo cat /etc/norad.d/relay.yml
relay:
  restart: "no"
  container_name: norad_relay
  image: norad-registry.cisco.com:5000/relay:latest
  volumes:
  - /etc/norad.d:/relay:rw
  - /var/run/docker.sock:/relay/docker.sock:rw

$> sudo relay fixup-config
Are you sure you want to regenerate the Relay Docker configuration file /etc/norad.d/relay.yml? Please enter 'yes' or 'no'.
yes
2017/10/11 15:57:55 Loading the Relay Docker config located at /etc/norad.d/relay.yml.
2017/10/11 15:57:55 Relay file appears to ben outdated.
2017/10/11 15:57:55 Attempting to load an outdated /etc/norad.d/relay.yml Relay Docker file.
2017/10/11 15:57:55 Generating new Relay Docker configuration file and writing to /etc/norad.d/relay.yml...
2017/10/11 15:57:55 Relay Docker configuration file successfully fixed and written to /etc/norad.d/relay.yml.

$> sudo cat /etc/norad.d/relay.yml
version: "3"
services:
  relay:
    restart: always
    container_name: norad_relay
    image: norad-registry.cisco.com:5000/relay:latest
    volumes:
    - /etc/norad.d:/relay:rw
    - /var/run/docker.sock:/relay/docker.sock:rw
    environment:
    - RELAY_MODE=production
    logging:
      driver: json-file
      options:
        max-file: "5"
        max-size: 20m

$> sudo relay fixup-config
Are you sure you want to regenerate the Relay Docker configuration file /etc/norad.d/relay.yml? Please enter 'yes' or 'no'.
yes
2017/10/11 15:59:37 Loading the Relay Docker config located at /etc/norad/relay.yml.
2017/10/11 15:59:37 Format of Relay configuration appears correct. Nothing to do.

$>
```

### Install the Relay

If you are using this utility on a system without an existing Relay (recommended), you can use the `install` command to set up your Relay.

```sh
$> sudo relay install --organization-token test_token_1234
2017/10/03 21:47:29 Validating install /etc/norad.d directory does not exist...
2017/10/03 21:47:29 OK
2017/10/03 21:47:29 Checking Docker version...
2017/10/03 21:47:29 Version 17.6.2-ce is supported.
2017/10/03 21:47:29 Downloading image norad-registry.cisco.com:5000/relay:latest... (this may take a while)
2017/10/03 21:47:32 Image successfully downloaded.
2017/10/03 21:47:32 Successfully created the directory /etc/norad.d.
2017/10/03 21:47:32 Generating Docker Compose file...
2017/10/03 21:47:32 Docker Compose file successfully written to /etc/norad.d/relay.yml.
2017/10/03 21:47:32 Generating Relay configuration file...
2017/10/03 21:47:32 Relay configuration file successfully written to /etc/norad.d/configuration.yml.
2017/10/03 21:47:32 Enabling the Norad Relay.
2017/10/03 21:47:32 Checking for an existing Norad Relay container.
2017/10/03 21:47:32 OK
2017/10/03 21:47:32 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
2017/10/03 21:47:32 Norad Relay successfully enabled!
```

### View Relay Status

The `status` command provides an overview of the currently Relay status, including what mode it is
running in (production or debug) and if an update is available.

```sh
2017/10/17 19:01:43 Attempting to detect installation in directory /etc/norad.d...
2017/10/17 19:01:43 The Relay appears to be installed!

2017/10/17 19:01:43 Checking if an update is available for the Relay...
2017/10/17 19:01:43 Connecting to the registry at https://norad-registry.cisco.com:5000 to look up the latest version...
2017/10/17 19:01:46 Comparing latest version to local version...
2017/10/17 19:01:46 You are running the latest version of the Relay!

2017/10/17 19:01:46 Checking if the Relay is enabled...
2017/10/17 19:01:46 Checking for an existing Norad Relay container.
2017/10/17 19:01:46 Norad Relay container f3c12ab68fefa5359bbaf3154952452edbbfa72e854fb11dca2828558545f25a found.
2017/10/17 19:01:46 The Relay is enabled!

2017/10/17 19:01:46 Checking if the Relay is running...
2017/10/17 19:01:46 Checking for an existing Norad Relay container.
2017/10/17 19:01:46 Norad Relay container f3c12ab68fefa5359bbaf3154952452edbbfa72e854fb11dca2828558545f25a found.
2017/10/17 19:01:46 The Relay is running in PRODUCTION mode!
```

### View Relay Key Fingerprint

To enable a Relay for use by Norad, you must verify that the Relay being displayed in the Norad UI
is indeed the one you added. This fingerprint is available in the Relay logs, and is also available
via the `fingerprint` convenience method.

```sh
$> sudo relay fingerprint
Full fingerprint:
  D9:DE:DD:BB:C7:90:2F:FB:96:E3:46:DA:CE:37:D0:E7:33:1A:00:5F:92:35:39:0E:45:EA:3F:5F:9F:27:E6:88
Truncated fingerprint:
  45:EA:3F:5F:9F:27:E6:88
```

### Viewing Relay Logs

The `logs` command shows (and optionally follows) the logs for the Norad Relay.

```sh
$> sudo relay logs -f
[Norad Relay Container Log] 2017-10-03 21:49:34 INFO norad: Relay::Api Creating client for
communicating with API
[Norad Relay Container Log] 2017-10-03 21:49:34 DEBUG norad: Relay::Api Sending heartbeat request to
API
[Norad Relay Container Log] 2017-10-03 21:49:34 DEBUG norad: Relay::Api Relay key fingerprint:
d9:de:dd:bb:c7:90:2f:fb:96:e3:46:da:ce:37:d0:e7:33:1a:00:5f:92:35:39:0e:45:ea:3f:5f:9f:27:e6:88
[Norad Relay Container Log] 2017-10-03 21:49:40 INFO norad: Relay::DockerQueue Info: Setting up
remote management...
[Norad Relay Container Log] W, [2017-10-03T21:49:40.688572 #1]  WARN -- #<Bunny::Session:0x1afec30
@norad.cisco.com:5671, vhost=, addresses=[norad.cisco.com:5671]>: Using TLS but no client
certificate is provided! If RabbitMQ is configured to verify peer
[Norad Relay Container Log] certificate, connection upgrade will fail!
[Norad Relay Container Log]
[Norad Relay Container Log] 2017-10-03 21:49:42 INFO norad: Relay::DockerQueue [*] Waiting for
commands on queue
^C
```

### Viewing Tests Started by the Relay

The `tests` command can show in progress (and, optionally, exited) tests. This can be particularly
useful in debug mode as you can also use the `logs` command to view the logs of the test you care
about by using the `--container-name TEST-ID` option.

```sh
$> sudo relay tests
2017/10/06 20:38:08 Looking for in progress Relay tests...
2017/10/06 20:38:08 No tests found.

$> sudo relay tests -a
2017/10/06 20:38:24 Looking for all Relay tests...

Test 1
        ID: noradjob_6db7c730f56c71fd9c27276b3be068a9f7dc1db3
        Status: Exited (0) 7 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 2
        ID: noradjob_f2e319676a5d090ba23fe1aef0ec188dab835130
        Status: Exited (0) 7 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 3
        ID: noradjob_c6134594367b511b3953b7fab0ad1e5602ebb7ac
        Status: Exited (0) 7 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 4
        ID: noradjob_47c3f85dfbc6beed179c02e5a22f168d754af0fa
        Status: Exited (0) 7 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 5
        ID: noradjob_445d3668e7c19d295270dad1160a070e9ea1fc04
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 6
        ID: noradjob_2377abbb3824e49c504277655b096e87e618612f
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 7
        ID: noradjob_c42f119fb87929875ef8eafbc9964498a0ca09bb
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 8
        ID: noradjob_8c14a4b1f186b681d1f16a96e6a617db32730630
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 9
        ID: noradjob_ccd73f33b340bcead06f0b06f820e58f9085a83d
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 10
        ID: noradjob_e13a0b47b215ac7fc762c758b4d5ee33532d61e1
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 11
        ID: noradjob_aa1468b42afaf1e556d41f03ebcdcddad60bf1d6
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 12
        ID: noradjob_3919b98e934873a0f80525f564a096b5dfd7af6d
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 13
        ID: noradjob_0f5c267558ea4f805aa4b0f0950097cf37da4ea8
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 14
        ID: noradjob_48d2ccdc1964a36347146336a97bf487b17e7351
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 15
        ID: noradjob_7968a5484748f444a349d59c663fc1a4e74b8e7e
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 16
        ID: noradjob_3cf728774880119d6cc1a18252f6b7f44a27e5f2
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 17
        ID: noradjob_4ed00dd46e9a5e5d949ff8c7ec9b7b8677e96c02
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 18
        ID: noradjob_83da19a5d876d2ec0a36291764ea39060056b794
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 19
        ID: noradjob_fa3d714d514f5fd8b11369a73261f932b297425a
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 20
        ID: noradjob_2d4087a37a4d17380b92cb3efa0bea5f225d1ded
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 21
        ID: noradjob_d77d762fea080ec3fe6da0ed65e6a29f686d6d88
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 22
        ID: noradjob_1ab39cdf5e9889361f7e333f213021715a205a60
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 23
        ID: noradjob_3167def229e6f254dadd6fcf3d017c4c726efb9f
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 24
        ID: noradjob_4a3c258c31bf0b051df3929e78bfe6e2bafe5c9a
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 25
        ID: noradjob_3f7fce4caf3a3586eef24cce9fe7a233b37c7f52
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 26
        ID: noradjob_ff1ac6c9a700d36dfb11fa7d29f9278b5b639220
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 27
        ID: noradjob_e81b1660463ef99889d4e1c088270408cb92e347
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 28
        ID: noradjob_a6f01818a433eefeb8c544c338f4c1af59334466
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 29
        ID: noradjob_345668f558bb2114f5930856920e7a53fd6a8b42
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 30
        ID: noradjob_20ad9540f5047c01aebe50db498dfe2f81456761
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

Test 31
        ID: noradjob_c4dde0ee3ce961bfb8aae1ae954dcf1414a6945e
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1

Test 32
        ID: noradjob_de6fc136eb097059418eb49acb11b173d8c384bf
        Status: Exited (0) 8 weeks ago
        Test: norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1

$> sudo relay logs --container-name noradjob_c4dde0ee3ce961bfb8aae1ae954dcf1414a6945e
[Norad Relay Container Log]
[Norad Relay Container Log] Starting Nmap 7.40 ( https://nmap.org ) at 2017-08-10 17:25 UTC
[Norad Relay Container Log] Nmap scan report for norad.cisco.com (52.14.176.79)
[Norad Relay Container Log] Host is up (0.027s latency).
[Norad Relay Container Log] rDNS record for 52.14.176.79: ec2-52-14-176-79.us-east-2.compute.amazonaws.com
[Norad Relay Container Log] PORT     STATE SERVICE
[Norad Relay Container Log] 8443/tcp open  https-alt
[Norad Relay Container Log] | ssl-dh-params:
[Norad Relay Container Log] |   NOT VULNERABLE:
[Norad Relay Container Log] |   Anonymous Diffie-Hellman Key Exchange MitM Vulnerability
[Norad Relay Container Log] |     State: NOT VULNERABLE
[Norad Relay Container Log] |     References:
[Norad Relay Container Log] |       https://www.ietf.org/rfc/rfc2246.txt
[Norad Relay Container Log] |
[Norad Relay Container Log] |   Transport Layer Security (TLS) Protocol DHE_EXPORT Ciphers Downgrade MitM (Logjam)
[Norad Relay Container Log] |     State: NOT VULNERABLE
[Norad Relay Container Log] |     IDs:  CVE:CVE-2015-4000  OSVDB:122331
[Norad Relay Container Log] |     References:
[Norad Relay Container Log] |       https://weakdh.org
[Norad Relay Container Log] |       http://osvdb.org/122331
[Norad Relay Container Log] |       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4000
[Norad Relay Container Log] |
[Norad Relay Container Log] |   Diffie-Hellman Key Exchange Insufficient Group Strength
[Norad Relay Container Log] |     State: NOT VULNERABLE
[Norad Relay Container Log] |     References:
[Norad Relay Container Log] |       https://weakdh.org
[Norad Relay Container Log] |
[Norad Relay Container Log] |   Diffie-Hellman Key Exchange Incorrectly Generated Group Parameters
[Norad Relay Container Log] |     State: NOT VULNERABLE
[Norad Relay Container Log] |     References:
[Norad Relay Container Log] |       http://www2.esentire.com/TLSUnjammedWP
[Norad Relay Container Log] |_      https://weakdh.org
[Norad Relay Container Log]
[Norad Relay Container Log] Nmap done: 1 IP address (1 host up) scanned in 4.00 seconds
```

### Updating the Relay

The CLI tool can be used to update the Norad Relay to its latest version.

```sh
$> sudo relay update
2017/10/03 21:54:59 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
Are you sure you want to update the Relay? Please enter 'yes' or 'no'.
yes
2017/10/03 21:55:01 Downloading image norad-registry.cisco.com:5000/relay:latest... (this may take a
while)
2017/10/03 21:55:03 Image successfully downloaded.
2017/10/03 21:55:03 Checking for an existing Norad Relay container.
2017/10/03 21:55:03 Norad Relay container
7a9146f7531163637fa1f169ea12a830871b7083ab00281c092ef3d7c9500671 found.
2017/10/03 21:55:03 Stopping the Norad Relay container...
2017/10/03 21:55:04 OK
2017/10/03 21:55:04 Removing the Norad Relay container...
2017/10/03 21:55:04 OK
2017/10/03 21:55:04 Checking for an existing Norad Relay container.
2017/10/03 21:55:04 OK
2017/10/03 21:55:04 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
2017/10/03 21:55:04 Norad Relay successfully enabled!
2017/10/03 21:55:04 Relay restarted!
```

### Enabling the Relay

**NOTE** By default, the Relay is enabled upon installation! If you disabled the Relay later or
elected to not automatically enable the Relay during installation, you can use the `enable` command.

An enabled Relay will restart automatically upon system reboots.

```sh
$> sudo relay enable
2017/10/03 21:49:33 Checking for an existing Norad Relay container.
2017/10/03 21:49:33 OK
2017/10/03 21:49:33 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
2017/10/03 21:49:34 Norad Relay successfully enabled!
```

### Disabling the Relay

To turn off and prevent the restarting of the Norad Relay, you can use the `disable` command.

```sh
$> sudo relay disable
Are you sure you want to disable the Relay? Please enter 'yes' or 'no'.
yes
2017/10/03 21:49:29 Checking for an existing Norad Relay container.
2017/10/03 21:49:29 Norad Relay container
e765cfdaa7f83544ea9c39b54b1ef36cdbd1cd8b218c5116df8e1e25e22ed211 found.
2017/10/03 21:49:29 Stopping the Norad Relay container...
2017/10/03 21:49:30 OK
2017/10/03 21:49:30 Removing the Norad Relay container...
2017/10/03 21:49:30 OK
```

### Toggling the Relay Debug Mode

By default the Relay removes test containers as they complete. If you'd like to inspect the test
containers after they complete, the Relay can be run in a debug mode. The CLI utility can help you
toggle the mode.

#### Enabling Debug Mode

```sh
$> sudo relay debug
Are you sure you want to enable the debug mode of the Relay? Please enter 'yes' or 'no'.
yes
2017/10/04 15:11:25 Checking for an existing Norad Relay container.
2017/10/04 15:11:25 Norad Relay container 0b60934dabdd4551b3e122b7d70cd35c6994409f7b775e6938b72c9a09bb946e found.
2017/10/04 15:11:25 Stopping the Norad Relay container...
2017/10/04 15:11:26 OK
2017/10/04 15:11:26 Removing the Norad Relay container...
2017/10/04 15:11:26 OK
2017/10/04 15:11:26 Checking for an existing Norad Relay container.
2017/10/04 15:11:26 OK
2017/10/04 15:11:26 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
2017/10/04 15:11:26 Norad Relay successfully enabled!
2017/10/04 15:11:26 Relay restarted!
2017/10/04 15:11:26 Relay mode successfully changed!
```

#### Disabling the Debug Mode
```sh
$> sudo relay debug --disable
Are you sure you want to disable the debug mode of the Relay? Please enter 'yes' or 'no'.
yes
2017/10/04 15:12:28 Debug mode setting found. Changing to production mode.
2017/10/04 15:12:28 Checking for an existing Norad Relay container.
2017/10/04 15:12:28 Norad Relay container 0ead0c0d45114c7dea3bb6252bbecc07edbb4cc32b2fe80d2ba0ca75aae5df51 found.
2017/10/04 15:12:28 Stopping the Norad Relay container...
2017/10/04 15:12:28 OK
2017/10/04 15:12:28 Removing the Norad Relay container...
2017/10/04 15:12:28 OK
2017/10/04 15:12:28 Checking for an existing Norad Relay container.
2017/10/04 15:12:28 OK
2017/10/04 15:12:28 Enabling the Relay using the /etc/norad.d/relay.yml Relay Docker file.
2017/10/04 15:12:30 Norad Relay successfully enabled!
2017/10/04 15:12:30 Relay restarted!
2017/10/04 15:12:30 Relay mode successfully changed!
```

### Restarting the Relay

The `restart` command can be used to restart the Norad Relay.

### Uninstalling the Relay

The `uninstall` command disables the Relay and removes all configuration files.

### Reseting the Relay

The `reset` command will uninstall and re-install the Relay with a new configuration. It does not
preserve data.
