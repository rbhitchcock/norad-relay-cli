package main

import (
	"gitlab.com/norad/relay-cli/relay"
	"gopkg.in/urfave/cli.v1"
)

// Initializes the Dependencies for Installation
func installHandler(c *cli.Context) error {
	org_token := c.String("organization-token")
	if org_token == "" {
		org_token = getOrgToken()
	}

	docker_client, err := relay.NewRelayDockerClient()
	if err != nil {
		panic(err)
	}

	installer := relay.RelayInstaller{
		Docker: relay.RelayDocker{
			Client:    docker_client,
			MinVer:    "1.13.0-0",
			ServerVer: getDockerServerVer(docker_client),
		},
		Config: relay.InstallConfig{
			InstallDir: c.String("dir"),
			DryRun:     dry_run,
			Enable:     !c.Bool("no-enable"),
		},
		Relay: relay.Relay{
			Image: c.String("relay-image"),
			Config: relay.RelayConfig{
				API_URI:                      c.String("api-uri"),
				Organization_Token:           org_token,
				Private_Key:                  "/relay/relay.pem",
				DB:                           "/relay/relay.store",
				Queue_User:                   c.String("queue-user"),
				Queue_Password:               c.String("queue-password"),
				Whitelisted_Repository_Hosts: []string{"norad-registry.cisco.com"},
			},
		},
	}
	return relay.Install(installer)
}
